const isEmpty = value => {
  const getString = value.toString();
  return getString.trim() === "" ? true : false;
};

const checkUndefinedInput = data => {
  let arr = [];
  let undefined = {};

  Object.entries(data).forEach(([key, value]) => {
    arr.push(`${key}:${value}`);
  });

  arr.forEach(val => {
    const splitVal = val.split(":");
    const key = splitVal[0];
    const value = splitVal[1];
    const text = `${key} is missing`;

    if (value === "undefined") {
      undefined[key] = text;
    }
  });

  return {
    isUndefined: Object.keys(undefined).length !== 0 ? true : false,
    undefined,
  };
};

const checkEmptyValue = data => {
  let arr = [];
  let errors = {};

  Object.entries(data).forEach(([key, value]) => {
    arr.push(`${key}:${value}`);
  });

  arr.forEach(val => {
    const splitVal = val.split(":");
    const key = splitVal[0];
    const value = splitVal[1];
    const text = `${key} is empty`;

    if (isEmpty(value)) errors[key] = text;
  });

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false,
  };
};

const validInput = data => {
  const { isUndefined, undefined } = checkUndefinedInput(data);
  if (isUndefined) {
    return {
      errors: { _type: "undefined", ...undefined },
      valid: !isUndefined,
    };
  } else {
    const { valid, errors } = checkEmptyValue(data);

    return {
      errors: { _type: "empty", ...errors },
      valid,
    };
  }
};

module.exports = validInput;
