const express = require("express");

const database = require("../database");
const query = require("knex")(database.MysqlConnection);
const { messages } = require("../util");
const { isvalidInput } = require("../validation");

const router = express.Router();

const date = new Date();

const initialRoute = (req, res) => {
  return res.status(200).json({ message: "Welcome to mail API si_pesat" });
};

// ----------------------------- Mail List -----------------------------
// Get All Mail List
const getMailList = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  try {
    const mailLists = await query("mail_list");
    return messages.successMessage(req, res, { data: mailLists });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};
// Get Mail List By Id
const getMailListById = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const list_id = req.params.id;
  try {
    const searchMailList = await query("mail_list").where({
      mail_list_id: list_id,
    });
    // when list return nothing
    if (searchMailList.length === 0) return messages.notFoundMessage(req, res);
    // when list return obj
    const mailList = searchMailList[0];

    return messages.successMessage(req, res, { data: mailList });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};
// End Mail List -------------------------------------------------------

// ----------------------------- Main Mail -----------------------------
// Send a mail
const sendNewMail = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  // data for database
  const db_data = {
    mail_list_id: req.body.mail_list_id,
    idfakultas: req.body.idfakultas,
    mail_nim: req.body.mail_nim,
    mail_mahasiswa: req.body.mail_mahasiswa,
    mail_ttl: req.body.mail_ttl,
    mail_alamat: req.body.mail_alamat,
    mail_noHp: req.body.mail_noHp,
    mail_progdi: req.body.mail_progdi,
    mail_smtak_thak: req.body.mail_smtak_thak,
    mail_option: req.body.mail_option,
    mail_ditunjukan: req.body.mail_ditunjukan,
    mail_alamatInstansi: req.body.mail_alamatInstansi,
    mail_matkul: req.body.mail_matkul,
    mail_anggotaKelompok: req.body.mail_anggotaKelompok,
    mail_created: date.getTime(),
  };
  const { errors, valid } = isvalidInput(db_data);
  if (!valid) return messages.badRequestMessage(req, res, { errors });

  try {
    const searchMailList = await query("mail_list").where({
      mail_list_id: db_data.mail_list_id,
    });
    // when list return nothing
    if (searchMailList.length === 0)
      return messages.notFoundMessage(req, res, {
        err: { mailListId: "not found" },
      });
    // when list return obj -> next step
    // Insert new data to db
    const result = await query("mail").insert(db_data);
    const mailId = result[0];

    const addStatusMail = addNewStatusMail(mailId);
    if (addStatusMail) return messages.successMessage(req, res);
    return messages.badRequestMessage(req, res);
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};
// End Main Mail -------------------------------------------------------

// ----------------------------- Mail Status -----------------------------
// Add New Status Mail
const addNewStatusMail = async mail_id => {
  const data = {
    mail_id,
    mail_status_status: "Diproses",
    mail_status_keterangan: "-",
    mail_status_created: date.getTime(),
  };
  try {
    const insertStatusMail = await query("mail_status").insert(data);
    if (insertStatusMail.length === 0) return false;
    else return true;
  } catch (error) {
    console.log(error);
  }
};

// Get Status Mail
const getStatusMailByNim = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const nim = req.mahasiswa.nim;

  try {
    const getStatus = await query("view_mail_status").where({ mail_nim: nim });
    if (getStatus.length === 0)
      return messages.notFoundMessage(req, res, {
        error: `Nim ${nim} mail_status is not found`,
      });

    return messages.successMessage(req, res, {
      data: getStatus,
    });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

// router get
router.get("/", initialRoute);
router.get("/list", getMailList);
router.get("/list/:id", getMailListById);
router.get("/status_mahasiswa", getStatusMailByNim);
// router post
router.post("/send", sendNewMail);

module.exports = router;
