const express = require("express");
const admin = require("./admin");
const mahasiswa = require("./mahasiswa");
const mail = require("./mail");

const router = express.Router();

router.get("/", (req, res) => {
  return res.status(200).json({ message: "Welcome to main API si_pesat" });
});

router.use("/mhs", mahasiswa);
router.use("/mail", mail);
router.use("/admin", admin);

module.exports = router;
