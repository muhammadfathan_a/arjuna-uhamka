const express = require("express");
const database = require("../database");
const query = require("knex")(database.MysqlConnection);
const { messages } = require("../util");
const jwt = require("jsonwebtoken");

const { isvalidInput } = require("../validation");

const router = express.Router();
const date = new Date();

const initialRoute = (req, res) => {
  return res.status(200).json({ message: "Welcome to admin API si_pesat" });
};

// ------------ Login Admin ------------
const loginAdmin = async (req, res) => {
  const data = {
    admin_name: req.body.admin_name,
    admin_password: req.body.admin_password,
  };

  const { errors, valid } = isvalidInput(data);
  if (!valid) return messages.badRequestMessage(req, res, { errors });
  try {
    const result = await query("admin").where({ admin_name: data.admin_name });

    // Get result data on array index 0
    const tempAdmin = result[0];
    if (result.length === 0)
      return messages.notFoundMessage(req, res, {
        errs: { msg: "Admin is not found" },
      });

    if (tempAdmin.admin_password !== data.admin_password)
      return messages.badRequestMessage(req, res, {
        errs: { msg: "Password is wrong, try again" },
      });

    const admin = { ...tempAdmin };
    admin.admin_password = null;

    const token = jwt.sign({ admin }, "uhamkasecrettoken", {
      expiresIn: "24h",
    });

    const tokenEks = date.getTime() + 24 * 60 * 60 * 1000;

    return messages.successMessage(req, res, {
      token,
      expToken: tokenEks,
      loginFor: "admin",
    });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

const getDataAdmin = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const admin = req.admin;
  return messages.successMessage(req, res, { data: admin });
};

// ----------- List Admin ------------------
const listAdmin = async (req, res) => {
  try {
    const getListAdmin = await query("admin").select("*");
    const list_admin = [];

    getListAdmin.map(list => {
      const tempList = { ...list };
      tempList.admin_password = null;
      list_admin.push(tempList);
    });

    return messages.successMessage(req, res, { data: list_admin });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

// ---------- Mail Author Admin ------------------
const getStatusMailByAdminFakultas = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const namafakultas = req.admin.admin_name;

  try {
    const result = await query("view_mail_status").where({ namafakultas });
    if (result.length === 0)
      return messages.notFoundMessage(req, res, {
        errs: { msg: "Mail not found" },
      });

    return messages.successMessage(req, res, { data: result });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

const updateStatusMailAdmin = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const mail_status_id = req.params.id;

  const data = {
    mail_status_status: req.body.mail_status_status,
    mail_status_keterangan: req.body.mail_status_keterangan,
  };

  const { errors, valid } = isvalidInput(data);
  if (!valid) return messages.badRequestMessage(req, res, { errors });

  try {
    const result = await query("mail_status").where({ mail_status_id });
    if (result.length === 0)
      return messages.notFoundMessage(req, res, {
        errs: { msg: "mail is not found" },
      });

    // Update mail_status -------
    await query("mail_status").update(data).where({ mail_status_id });
    // After update, query again -------
    const resultUpdate = await query("mail_status").where({ mail_status_id });
    const mail_status = resultUpdate[0];

    return messages.successMessage(req, res, { data: mail_status });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};
// -----------------------------------------

// ------- Change Password Admin ----------
const changePasswordAdmin = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);
  const admin_id = req.admin.admin_id;

  const data = {
    admin_password: req.body.new_password,
    re_password: req.body.re_password,
  };

  const { errors, valid } = isvalidInput(data);

  if (!valid) return messages.badRequestMessage(req, res, { errors });

  try {
    const resultAdmin = await query("admin").where({ admin_id });
    if (resultAdmin.length === 0)
      return messages.badRequestMessage(req, res, {
        errs: { msg: "admin is not found" },
      });

    if (data.admin_password !== data.re_password)
      return messages.badRequestMessage(req, res, {
        errs: { msg: "password is not match" },
      });

    await query("admin")
      .update({ admin_password: data.admin_password })
      .where({ admin_id });
    return messages.successMessage(req, res, {
      data: { msg: "success update password" },
    });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

router.get("/", initialRoute);
router.get("/data_admin", getDataAdmin);
router.get("/list_admin", listAdmin);
router.get("/status_mail", getStatusMailByAdminFakultas);
// router post admin
router.post("/login", loginAdmin);
router.post("/status_mail/update/:id", updateStatusMailAdmin);
router.post("/change_password", changePasswordAdmin);

module.exports = router;
