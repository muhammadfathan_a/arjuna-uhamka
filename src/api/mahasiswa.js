const express = require("express");
const axios = require("axios");
const jwt = require("jsonwebtoken");

const database = require("../database");
// Query From Database Local
const query = require("knex")(database.MysqlConnection);
// Query From Database UHAMKA
const queryUHAMKA = require("knex")(database.MSSQLConnection);
// Util
const { messages } = require("../util");
const { isvalidInput } = require("../validation");

const router = express.Router();
const date = new Date();

const initialRoute = (req, res) => {
  return res.status(200).json({ message: "Welcome to MHS API si_pesat" });
};

const getFakultasProdi = async ({ idprodi, idfakultas }) => {
  try {
    const fakultas_prodi = await query("view_fakultas_prodi").where({
      idprodi,
      idfakultas,
    });
    return fakultas_prodi;
  } catch (error) {
    console.log(error);
  }
};

const login = async (req, res) => {
  const data = {
    nim: req.body.nim,
    pwd: req.body.pwd,
  };

  const { valid, errors } = isvalidInput(data);
  if (!valid) return messages.badRequestMessage(req, res, { errors });

  try {
    const result = await axios({
      method: "post",
      url: "https://wisuda.uhamka.ac.id/util/api/login/mhs",
      data,
    });
    // Get result data on array index 0
    const mahasiswa = result.data[0];

    if (mahasiswa.nama === null)
      return messages.notFoundMessage(req, res, {
        errs: { msg: "Mahasiswa is not found", err: mahasiswa },
      });

    if (mahasiswa.er === "false")
      return messages.badRequestMessage(req, res, {
        errs: { msg: "Password is wrong, try again" },
      });

    const fakultasProdi = await getFakultasProdi({
      idfakultas: mahasiswa.kodefak,
      idprodi: mahasiswa.kodeprogdi,
    });

    const namaprodi = fakultasProdi[0].namaprodi;
    const namafakultas = fakultasProdi[0].namafakultas;

    const data_mahasiswa = { ...mahasiswa };
    data_mahasiswa.namaprogdi = namaprodi;
    data_mahasiswa.namafakultas = namafakultas;

    const token = jwt.sign({ mahasiswa: data_mahasiswa }, "uhamkasecrettoken", {
      expiresIn: "24h",
    });

    const tokenEks = date.getTime() + 24 * 60 * 60 * 1000;

    return messages.successMessage(req, res, {
      token,
      expToken: tokenEks,
      loginFor: "mahasiswa",
    });
  } catch (error) {
    return messages.internalErrorMessage(req, res, { error });
  }
};

const getDataMahasiswa = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);

  const mahasiswa = req.mahasiswa;
  return messages.successMessage(req, res, { data: mahasiswa });
};

const searchMahasiswa = async (req, res) => {
  if (!req.isAuth) return messages.unAuthorizationMessage(req, res);
  const NIM = req.params.nim;
  const kodeprogdi = req.mahasiswa.kodeprogdi;
  try {
    const result = await queryUHAMKA("dbo.V_MHS").where({
      NIM,
      Expr1: kodeprogdi,
    });
    if (result.length === 0) {
      return messages.notFoundMessage(req, res, {
        error: { msg: "Mahasiswa is not found" },
      });
    }

    const tempMahasiswa = result[0];

    const mahasiswa = {
      KODEPROGDI: tempMahasiswa.Expr1,
      KODEFAK: tempMahasiswa.KODEFAK,
      NAMA: tempMahasiswa.NAMA,
      NIM: tempMahasiswa.NIM,
      NAMAFAK: tempMahasiswa.NAMAFAK,
      NAMAPROGDI: tempMahasiswa.NAMAPROGDI,
      TGLAHIR: tempMahasiswa.TGLAHIR,
      ANGKATAN: tempMahasiswa.ANGKATAN,
    };

    return messages.successMessage(req, res, {
      data: mahasiswa,
    });
  } catch (error) {
    console.log(error);
    return messages.internalErrorMessage(req, res, { error });
  }
};

// ----------- Route --------------
// Router Get ---------------
router.get("/", initialRoute);
router.get("/data_mahasiswa", getDataMahasiswa);
router.get("/search/:nim", searchMahasiswa);
// Router Post --------------
router.post("/login", login);

module.exports = router;
