const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");
const middlewares = require("./middlewares");
const api = require("./api");

const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(helmet());

app.use(express.json());
app.use(middlewares.authentication);

app.get("/", (req, res) => {
  return res.status(200).json({ message: `Welcome to si_pesat API 🔥` });
});

app.use("/si_pesat/api", api);
app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
