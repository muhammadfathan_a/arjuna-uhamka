require("dotenv").config();

const MysqlConnection = {
  client: "mysql",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  },
};

const MSSQLConnection = {
  client: "mssql",
  connection: {
    server: process.env.DB_SERVER_UHAMKA,
    user: process.env.DB_USER_UHAMKA,
    password: process.env.DB_PASSWORD_UHAMKA,
    database: process.env.DB_DATABASE_UHAMKA,
    options: {
      encrypt: false,
      enableArithAbort: true,
    },
  },
};

module.exports = {
  MysqlConnection,
  MSSQLConnection,
};
