-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2021 at 08:29 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uhamka_sipesat`
--

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `idfakultas` varchar(11) NOT NULL,
  `namafakultas` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`idfakultas`, `namafakultas`) VALUES
('01', 'KEGURUAN DAN ILMU PENDIDIKAN'),
('02', 'EKONOMI DAN BISNIS'),
('03', 'FARMASI DAN SAINS'),
('04', 'TEKNIK'),
('05', 'ILMU SOSIAL DAN ILMU POLITIK'),
('06', 'ILMU-ILMU KESEHATAN'),
('07', 'AGAMA ISLAM'),
('08', 'PSIKOLOGI'),
('09', 'PASCASARJANA'),
('10', 'KEDOKTERAN');

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE `mail` (
  `mail_id` int(11) NOT NULL,
  `mail_list_id` int(11) NOT NULL,
  `mail_nim` int(11) NOT NULL,
  `mail_mahasiswa` varchar(50) NOT NULL,
  `mail_ttl` varchar(50) NOT NULL,
  `mail_progdi` varchar(25) NOT NULL,
  `mail_smtak_thak` varchar(25) NOT NULL,
  `mail_alamat` varchar(255) NOT NULL,
  `mail_noHp` varchar(16) NOT NULL,
  `mail_option` varchar(10) NOT NULL,
  `mail_ditunjukan` varchar(100) NOT NULL,
  `mail_alamatInstansi` varchar(255) NOT NULL,
  `mail_matkul` varchar(50) NOT NULL,
  `mail_waktuPelaksanaan` varchar(50) NOT NULL,
  `mail_anggotaKelompok` text NOT NULL,
  `mail_created` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` (`mail_id`, `mail_list_id`, `mail_nim`, `mail_mahasiswa`, `mail_ttl`, `mail_progdi`, `mail_smtak_thak`, `mail_alamat`, `mail_noHp`, `mail_option`, `mail_ditunjukan`, `mail_alamatInstansi`, `mail_matkul`, `mail_waktuPelaksanaan`, `mail_anggotaKelompok`, `mail_created`) VALUES
(19, 7, 1703015175, 'Achmad Sufyan Aziz', 'Jakarta, 9 November 1998', 'Elektro', 'V / 2019', 'Jl Damai Jakarta', '08123456789', 'Individu', 'PT Garuda Indonesia', 'Jl. Kebon Sirih No. 44, Jakarta 10110, Indonesia.', 'PKL', 'NULL', 'NULL', 1610561013666),
(20, 5, 1703015175, 'Achmad Sufyan Aziz', 'Jakarta, 9 November 1998', 'Informatika', 'V / 2019', 'Jl Damai Jakarta', '08123456789', 'Individu', 'PT Gojek Indonesia', 'Jl. Kemang Timur No. 21', 'riset teknologi', 'NULL', 'NULL', 1610561210445),
(21, 2, 1703015175, 'Achmad Sufyan Aziz', 'Jakarta, 9 November 1998', 'Elektro', 'V / 2019', 'Jl Damai Jakarta', '0812345678910', 'Kelompok', 'PT Tokopedia', 'Jl. Raya Perjuangan No.12A, Kebon Jeruk, Kota Jakarta Barat, DKI Jakarta', 'E-Commerce', '1 Januari - 1 Februari 2021', '[{\"mahasiswa\":\"Rifqi Maulatur Rahman,1703015199\"},{\"mahasiswa\":\"Yudha Adhi P,1703015188\"},{\"mahasiswa\":\"Arip Afdal, 1703015177\"}]', 1610561210445);

-- --------------------------------------------------------

--
-- Table structure for table `mail_list`
--

CREATE TABLE `mail_list` (
  `mail_list_id` int(11) NOT NULL,
  `mail_list_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mail_list`
--

INSERT INTO `mail_list` (`mail_list_id`, `mail_list_name`) VALUES
(1, 'Surat Keterangan (Individu/Kelompok)'),
(2, 'Surat Izin Penelitian (Individu/Kelompok)'),
(3, 'Surat Keterangan Lulus (Individu)'),
(4, 'Surat Observasi (Individu/Kelompok)'),
(5, 'Surat Uji Validitas (Individu)'),
(6, 'Surat Cuti (Individu)'),
(7, 'Surat PKL (Individu/Kelompok)');

-- --------------------------------------------------------

--
-- Table structure for table `mail_status`
--

CREATE TABLE `mail_status` (
  `mail_status_id` int(11) NOT NULL,
  `mail_id` int(11) NOT NULL,
  `mail_status_status` varchar(16) NOT NULL,
  `mail_status_keterangan` varchar(255) NOT NULL,
  `mail_status_created` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mail_status`
--

INSERT INTO `mail_status` (`mail_status_id`, `mail_id`, `mail_status_status`, `mail_status_keterangan`, `mail_status_created`) VALUES
(17, 19, 'proses', 'Sedang di proses oleh pihak sekret', 1610560498379),
(18, 20, 'terkirim', '-', 1610561210445),
(19, 21, 'terkirim', '-', 1610561210445);

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `idprodi` varchar(11) NOT NULL,
  `idfakultas` varchar(11) NOT NULL,
  `namaprodi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`idprodi`, `idfakultas`, `namaprodi`) VALUES
('0112', '01', 'PENDIDIKAN GURU SEKOLAH DASAR'),
('0115', '01', 'PENDIDIKAN KONSELING'),
('0116', '01', 'PENDIDIKAN GURU PENDIDIKAN ANAK USIA DINI'),
('0122', '01', 'PENDIDIKAN BAHASA DAN SASTRA INDONESIA'),
('0123', '01', 'PENDIDIKAN BAHASA INGGRIS'),
('0124', '01', 'PENDIDIKAN BAHASA JEPANG'),
('0132', '01', 'PENDIDIKAN SEJARAH'),
('0133', '01', 'PENDIDIKAN GEOGRAFI'),
('0134', '01', 'PENDIDIKAN EKONOMI'),
('0142', '01', 'PENDIDIKAN MATEMATIKA'),
('0143', '01', 'PENDIDIKAN FISIKA'),
('0144', '01', 'PENDIDIKAN BIOLOGI'),
('0211', '02', 'AKUNTANSI (S1)'),
('0221', '02', 'MANAJEMEN'),
('0261', '02', 'PERPAJAKAN'),
('0271', '02', 'AKUNTANSI (D3)'),
('0291', '02', 'EKONOMI ISLAM'),
('0311', '03', 'FARMASI'),
('0312', '03', 'PROGRAM PROFESI APOTEKER'),
('0321', '03', 'D4 ANALIS KESEHATAN'),
('0411', '04', 'INFORMATIKA'),
('0421', '04', 'ELEKTRO'),
('0432', '04', 'MESIN'),
('0511', '05', 'ILMU KOMUNIKASI'),
('0611', '06', 'KESEHATAN MASYARAKAT'),
('0621', '06', 'GIZI'),
('0711', '07', 'PENDIDIKAN AGAMA ISLAM'),
('0721', '07', 'PERBANKAN SYARIAH'),
('0731', '07', 'PENDIDIKAN BAHASA ARAB'),
('0811', '08', 'PSIKOLOGI'),
('0911', '09', 'PENELITIAN DAN EVALUASI PENDIDIKAN'),
('0921', '09', 'MANAJEMEN'),
('0931', '09', 'ADMINISTRASI PENDIDIKAN'),
('0941', '09', 'PENDIDIKAN BAHASA INDONESIA'),
('0942', '09', 'PENDIDIKAN BAHASA INGGRIS'),
('0951', '09', 'ILMU KESEHATAN MASYARAKAT'),
('0961', '09', 'PENDIDIKAN IPS'),
('0971', '09', 'PENDIDIKAN DASAR'),
('0972', '09', 'PENDIDIKAN MATEMATIKA'),
('1001', '10', 'PENDIDIKAN DOKTER'),
('1003', '10', 'VOKASI TEKNIK KARDIOVASKULER (D3)');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_fakultas_prodi`
-- (See below for the actual view)
--
CREATE TABLE `view_fakultas_prodi` (
`idfakultas` varchar(11)
,`idprodi` varchar(11)
,`namafakultas` varchar(150)
,`namaprodi` varchar(200)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mail`
-- (See below for the actual view)
--
CREATE TABLE `view_mail` (
`mail_id` int(11)
,`mail_list_id` int(11)
,`mail_list_name` varchar(50)
,`mail_nim` int(11)
,`mail_mahasiswa` varchar(50)
,`mail_ttl` varchar(50)
,`mail_progdi` varchar(25)
,`mail_smtak_thak` varchar(25)
,`mail_alamat` varchar(255)
,`mail_noHp` varchar(16)
,`mail_option` varchar(10)
,`mail_ditunjukan` varchar(100)
,`mail_alamatInstansi` varchar(255)
,`mail_matkul` varchar(50)
,`mail_waktuPelaksanaan` varchar(50)
,`mail_anggotaKelompok` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mail_status`
-- (See below for the actual view)
--
CREATE TABLE `view_mail_status` (
`mail_status_id` int(11)
,`mail_id` int(11)
,`mail_list_id` int(11)
,`mail_status_status` varchar(16)
,`mail_status_keterangan` varchar(255)
,`mail_list_name` varchar(50)
,`mail_nim` int(11)
,`mail_mahasiswa` varchar(50)
,`mail_ttl` varchar(50)
,`mail_progdi` varchar(25)
,`mail_smtak_thak` varchar(25)
,`mail_alamat` varchar(255)
,`mail_noHp` varchar(16)
,`mail_option` varchar(10)
,`mail_ditunjukan` varchar(100)
,`mail_alamatInstansi` varchar(255)
,`mail_matkul` varchar(50)
,`mail_waktuPelaksanaan` varchar(50)
,`mail_anggotaKelompok` text
,`mail_created` bigint(20)
);

-- --------------------------------------------------------

--
-- Structure for view `view_fakultas_prodi`
--
DROP TABLE IF EXISTS `view_fakultas_prodi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_fakultas_prodi`  AS SELECT `p`.`idfakultas` AS `idfakultas`, `p`.`idprodi` AS `idprodi`, `f`.`namafakultas` AS `namafakultas`, `p`.`namaprodi` AS `namaprodi` FROM (`prodi` `p` join `fakultas` `f` on(`f`.`idfakultas` = `p`.`idfakultas`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_mail`
--
DROP TABLE IF EXISTS `view_mail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mail`  AS SELECT `m`.`mail_id` AS `mail_id`, `m`.`mail_list_id` AS `mail_list_id`, `ml`.`mail_list_name` AS `mail_list_name`, `m`.`mail_nim` AS `mail_nim`, `m`.`mail_mahasiswa` AS `mail_mahasiswa`, `m`.`mail_ttl` AS `mail_ttl`, `m`.`mail_progdi` AS `mail_progdi`, `m`.`mail_smtak_thak` AS `mail_smtak_thak`, `m`.`mail_alamat` AS `mail_alamat`, `m`.`mail_noHp` AS `mail_noHp`, `m`.`mail_option` AS `mail_option`, `m`.`mail_ditunjukan` AS `mail_ditunjukan`, `m`.`mail_alamatInstansi` AS `mail_alamatInstansi`, `m`.`mail_matkul` AS `mail_matkul`, `m`.`mail_waktuPelaksanaan` AS `mail_waktuPelaksanaan`, `m`.`mail_anggotaKelompok` AS `mail_anggotaKelompok` FROM (`mail` `m` join `mail_list` `ml` on(`ml`.`mail_list_id` = `m`.`mail_list_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_mail_status`
--
DROP TABLE IF EXISTS `view_mail_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mail_status`  AS SELECT `ms`.`mail_status_id` AS `mail_status_id`, `m`.`mail_id` AS `mail_id`, `ml`.`mail_list_id` AS `mail_list_id`, `ms`.`mail_status_status` AS `mail_status_status`, `ms`.`mail_status_keterangan` AS `mail_status_keterangan`, `ml`.`mail_list_name` AS `mail_list_name`, `m`.`mail_nim` AS `mail_nim`, `m`.`mail_mahasiswa` AS `mail_mahasiswa`, `m`.`mail_ttl` AS `mail_ttl`, `m`.`mail_progdi` AS `mail_progdi`, `m`.`mail_smtak_thak` AS `mail_smtak_thak`, `m`.`mail_alamat` AS `mail_alamat`, `m`.`mail_noHp` AS `mail_noHp`, `m`.`mail_option` AS `mail_option`, `m`.`mail_ditunjukan` AS `mail_ditunjukan`, `m`.`mail_alamatInstansi` AS `mail_alamatInstansi`, `m`.`mail_matkul` AS `mail_matkul`, `m`.`mail_waktuPelaksanaan` AS `mail_waktuPelaksanaan`, `m`.`mail_anggotaKelompok` AS `mail_anggotaKelompok`, `m`.`mail_created` AS `mail_created` FROM ((`mail_status` `ms` join `mail` `m` on(`m`.`mail_id` = `ms`.`mail_id`)) join `mail_list` `ml` on(`ml`.`mail_list_id` = `m`.`mail_list_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`idfakultas`);

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `mail_list`
--
ALTER TABLE `mail_list`
  ADD PRIMARY KEY (`mail_list_id`);

--
-- Indexes for table `mail_status`
--
ALTER TABLE `mail_status`
  ADD PRIMARY KEY (`mail_status_id`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`idprodi`),
  ADD KEY `idfakultas` (`idfakultas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `mail_list`
--
ALTER TABLE `mail_list`
  MODIFY `mail_list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mail_status`
--
ALTER TABLE `mail_status`
  MODIFY `mail_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
