const successMessage = (req, res, callback) => {
  return res.status(200).json({
    status: 200,
    message: "Success",
    timestamp: new Date().getTime(),
    ...callback,
  });
};

const badRequestMessage = (req, res, callback) => {
  return res.status(400).json({
    status: 400,
    message: "Something went wrong",
    timestamp: new Date().getTime(),
    ...callback,
  });
};

const unAuthorizationMessage = (req, res, callback) => {
  return res.status(401).json({
    status: 401,
    message: "Unauthorization or missing token",
    timestamp: new Date().getTime(),
    ...callback,
  });
};

const notFoundMessage = (req, res, callback) => {
  return res.status(404).json({
    status: 404,
    message: "Something is not found",
    timestamp: new Date().getTime(),
    ...callback,
  });
};
const internalErrorMessage = (req, res, callback) => {
  return res.status(500).json({
    status: 500,
    message: "Something went wrong with internal",
    timestamp: new Date().getTime(),
    ...callback,
  });
};

module.exports = {
  successMessage,
  badRequestMessage,
  unAuthorizationMessage,
  notFoundMessage,
  internalErrorMessage,
};
