import {React} from "libraries";
import DataTable from "react-data-table-component";
import {useState, useEffect} from 'react'
import {useHistory, useLocation} from 'react-router-dom'
import {Modal} from "components"
import { ImSearch} from "react-icons/im"; 
import {Link} from 'react-router-dom';
import {useForm} from "contexts/formContext";
import {useAdmin} from "contexts/adminContext";
import { getSlugMail } from "util/mail-list";


const TableOnProses = (props) => {

  // console.log(props.allMails);
  

    const {event : {setMailToInput}} = useForm();

  // Modal
  const [isShow, setShow] = useState(false)
  const [dataModal, setDataModal] = useState({})

  const openModal = async (mail_status_id) => {
    setShow(true)
    // const dataRow = lampiranUser.filter(row => row.id === id )
    const dataRow = props.allMails.filter(row => row.mail_status_id === mail_status_id )
    // console.log(dataRow)
    await setDataModal(dataRow[0])
  }

  const closeModal = () =>{
    setShow(false)
    setDataModal({})
  }
  // console.log(dataModal.mail_ditunjukan)
  // Modal End

  //Update
  const [addNote, setAddNote] = useState("-");
  const [addStatus, setStatus] = useState("-");
  const mail_id = dataModal.mail_status_id

  const handlerUpdateMail = useAdmin();
  // console.log(handlerUpdateMail.status)
  const handlerAddNote = (mail_id) =>{
    handlerUpdateMail.event.updateStatus({
        mail_status_id : mail_id,
        mail_status_status: addStatus,
        mail_status_keterangan: addNote
    })
      window.location.reload()
      setShow(false)
      setDataModal({})

  } 

  // paginaation
  const costumeStyle = {
    pagination : {
      style : {
        borderRadius : '0px 0px 10px 10px'
      }
    }
  }

  const history = useHistory();
  const location = useLocation();
  const queryPage = location.search.match(/page=([0-9]+)/, '');
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1);
  const [page, setPage] = useState(currentPage)

  const pageChange = (newPage) => {
    currentPage !== newPage && history.push(`/admin?page=${newPage}`)
  }

  useEffect(()=>{
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])
  // Pagination End

  
  const coloums = React.useMemo(()=>
  [
    {
      name:"Nama",
      selector:'mail_mahasiswa',
      sortable : true,
      grow : 2,
      cell : data => <div>{data.mail_mahasiswa}</div>
    },
    {
      name:"Nim",
      selector:'mail_nim',
      sortable : true,
      grow: 2,
      cell : data => <div>{data.mail_nim}</div>
    },
    {
      name:"Nama Surat",
      selector:'mail_list_name',
      sortable : true,
      grow : 2,
      cell : data =><div style={{color: 'black', overflow:'hidden',textOverflow: 'ellipsis'}}>{data.mail_list_name}</div>
    },
    {
      name:"Di Tujukan",
      selector:'mail_ditujukan',
      sortable : true,
      cell : data => <div>{data.mail_ditunjukan}</div>

    },
    {
      name:"Jenis Surat",
      selector:'mail_option',
      sortable : true,
      cell : data => <div>{data.mail_option}</div>

    },
    {
      name:"Status",
      selector:'mail_status_status',
      sortable : true,
      cell : data => <div>{data.mail_status_status}</div>

    },
    {
      name: "Action",
      grow: 3,
      cell : (p) =>{
        // console.log("isi p = ",p)
        const slug = getSlugMail(p.mail_list_name);
        return(
          <React.Fragment>
            <Link
              className="download_btn"
              to={`/a/surat/surat${slug}.pdf`}
              onClick={()=> setMailToInput(p)}
            >
              Download
            </Link>
            <button
              onClick={() => openModal(p.mail_status_id)}
              className="note__btn"
            >
              Catatan
            </button>
          </React.Fragment>
        )
      }
    }


  ])

    const [resetPaginationToggle, setResetPagination] = useState(false);

    // Filter Data 
    const [filterText, setFilterText] = useState('');

    const searchData = () =>{
      const itemData = props.filter[0] && Object.keys(props.filter[0])
      return props.filter.filter(
        (item) =>
          itemData.some((itemData) =>item[itemData].toString().toLowerCase().indexOf(filterText.toLowerCase()) !== -1 )

          // item.mail_nim.toString().toLowerCase().indexOf(filterText.toLowerCase()) !== -1 ||
          // item.mail_ditunjukan.toString().toLowerCase().indexOf(filterText.toLowerCase()) !== -1 ||
          // item.mail_mahasiswa.toString().toLowerCase().indexOf(filterText.toLowerCase()) !== -1
        )
    }
    //End Filter Data
  return(
    <React.Fragment>
      <div className="search__form">
        <input
          className="search__input"
          type="text"
          placeholder="Cari Dengan Nim" 
          // value={filterText}
          onChange={(event) => setFilterText(event.target.value)}
        />
        <button className="search__btn" type="button">
          <ImSearch />
        </button>
      </div>
        <DataTable
          Title= "List Permohonan"
          noHeader
          columns={coloums}
          // data={dataSearch(filterText)}
          data={searchData()}
          activePage={page}
          paginationResetDefaultPage={resetPaginationToggle}
          onChangePage={pageChange}
          pagination
          responsive={true}
          pages={5}
          paginationPerPage={5}
          customStyles={costumeStyle}
          paginationRowsPerPageOptions={[5,10,15,20,25,30]}
          striped
          highlightOnHover
        />
        {isShow ? <div onClick={closeModal} className="modal__backdrop"/> : null}
        <Modal 
          show={isShow}
        >
          <div className="status">
            <div className="status__mail">
              <select
                id= "status"
                className="form__status"
                onChange={e => setStatus(e.target.value)}
              >
                <option defaultValue>Masukan Status Surat</option>
                <option value="Diproses">Di Proses</option>
                <option value="Diterima">Di Terima</option>
                <option value="Ditolak">Di Tolak</option>
              </select>
            </div>

              <textarea className="addNote" onChange={e => setAddNote(e.target.value)} />

            <div className="btn__addNote">
              <button 
                className="btn__addNote btn--darkBlue"
                onClick={() => handlerAddNote(mail_id)}
              >
                  Tambahkan
              </button>
            </div>
          </div>
        </Modal>
    </React.Fragment>
  )
}

export default TableOnProses;