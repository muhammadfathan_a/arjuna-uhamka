import LoginForm from './LoginForm';
import NavbarAdmin from './NavbarAdmin';
import TableOnProses from './TableOnProses';
import TableCompleted from './TableCompleted';
import TableFailed from './TableFailed';

export {
  LoginForm,
  NavbarAdmin,
  TableOnProses,
  TableCompleted,
  TableFailed
}