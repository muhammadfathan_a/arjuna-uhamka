import React from "libraries";
import { useState } from "react";
import { useUser } from "../../../contexts/userContext";
import { useAdmin } from "../../../contexts/adminContext";
import fakultas from "../../../assets/fakultas/fakultas.json"
import Spinner from "components/global/Spinner";
import Alert from "components/global/Alert";

const LoginForm = ({ history }) => {

  const [user, setUser] = useState("KEGURUAN DAN ILMU PENDIDIKAN");
  const [isAdmin, setIsAdmin] = useState(false);
  const[pwd, setPassword] = useState("")

  const handleChangeType = () =>{
    setIsAdmin(!isAdmin)
    setPassword("")
  }
  // handlelogin for user
  const handlerLogin = useUser();

  //handle login for admin
  const handlerLoginAdmin = useAdmin();

  const errorMessage = () => {
    if (handlerLogin.errors.status === 500) {
      return handlerLogin.errors.message;
    } else if (handlerLogin.errors.status === 400) {
      return handlerLogin.errors.errs.msg;
    } else if (handlerLogin.errors.status === 404) {
      return handlerLogin.errors.errs.msg;
    }
  };

  const BtnLogin = () => {

    if (isAdmin){
      return handlerLoginAdmin.event.adminLogin({admin_name:user, admin_password: pwd, history})
    }else{
      return handlerLogin.event.userLogin({ nim:user, pwd, history });
    }
  };
  return (
    <div className="login__form">
      <div className="form__header">
        <div className="form__title">
          <h1>Login</h1>
        </div>
      </div>
      <div className="form">
        <div>
          {Object.keys(handlerLogin.errors).length === 0 ? (
            <div />
          ) : (
            <Alert type="errors" text={errorMessage()} />
          )}
        </div>
        <div className="form__group">
          <select
            // value={userTypes}
            id="usertype"
            className="form__control"
            onChange={handleChangeType}
          >
            <option value="mahasiswa">Mahasiswa</option>
            <option value="admin">Admin</option>
          </select>
        </div>
        <div className="form__group">
          {
            isAdmin  ? 
              <select
              id="usertype"
              className="form__control"
              onChange={e => setUser(e.target.value)}
            >
              {fakultas.map((fakultas,index) =>{
                return(
                  <option key={index} value={fakultas.fakultas}>{fakultas.text}</option>
                )
              })}
            </select> 
          :
        <input
          className="form__control"
          type="text"
          placeholder="Masukan Nim Anda"
          onChange={e=> setUser(e.target.value)}
        />
        }
        </div>
        <div className="form__group">
          <input
            className="form__control"
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)} value={pwd}
          />
        </div>
        {handlerLogin.loadingUser ? (
          <div className="form__group">
            <button
              className="btn__submit"
              onClick={BtnLogin}
              disabled={true}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Spinner size="10px" type="beat" color="var(--white)" />
            </button>
          </div>
        ) : (
          <div className="form__group">
            <button
              className="btn__submit"
              // onClick={() => handlerLogin.event.userLogin({nim,pwd})}
              onClick={BtnLogin}
            >
              Login
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default LoginForm;
