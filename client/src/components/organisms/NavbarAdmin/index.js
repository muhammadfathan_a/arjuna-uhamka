import {React} from 'libraries';
import { useAdmin} from "../../../contexts/adminContext";


const NavbarAdmin = () => {
  
  const handlerLogout = useAdmin();
  const {detailAdmin} =  useAdmin();
  
  const btnLogout = () => {
    const alert = window.confirm("Apa kamu ingin keluar?")
    if(alert){
      handlerLogout.event.adminLogout({})
    }
  }
  return(
      <div className="navbar__inner">
        <div className="navbar__admin">
          <div className="admin__name">
            FAKULTAS {detailAdmin.admin_name}
          </div>
          <div className="navbar__links">
            <a 
              className="link"
              href="/"
              onClick={btnLogout}
            >
              Logout
            </a>
          </div>
        </div>
      </div>
  )
}


export default NavbarAdmin