import React from "react";
import { useForm } from "contexts/formContext";
import { PDFViewer } from "@react-pdf/renderer";
import { TemplateSurat_First as TEMPLATE_SURAT_FIRST } from "./templates";

const GenerateSurat = ({ children }) => {
  const { form } = useForm();

  return (
    <div style={{ height: "100vh" }}>
      <PDFViewer style={{ width: "100%", height: "100%" }}>
        <TEMPLATE_SURAT_FIRST children={children} form={form} />
      </PDFViewer>
    </div>
  );
};
export default GenerateSurat;
