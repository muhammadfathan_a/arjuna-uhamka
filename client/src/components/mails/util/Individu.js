import GenerateSurat from "../GenerateSurat";
import { View, Text, StyleSheet } from "@react-pdf/renderer";
import { formatString } from "util/format";

const Individu = ({ form }) => {
  const { perihal } = form;
  const {
    mail_nim,
    mail_mahasiswa,
    mail_alamat,
    mail_noHp,
    mail_ttl,
    mail_matkul,
    mail_smtak_thak,
    mail_progdi,
    namafakultas,
  } = form.inputs;

  return (
    <GenerateSurat>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>Nama</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>
          {formatString(mail_mahasiswa).toCapitalizeEachWord()}
        </Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>Tempat/Tgl.Lahir</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>{mail_ttl}</Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>NIM</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>{mail_nim}</Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>Program Studi</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>
          {formatString(mail_progdi).toCapitalizeEachWord()}
        </Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>Semester/Th. Akademik</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>{mail_smtak_thak}</Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>Alamat</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>{mail_alamat}</Text>
      </View>
      <View style={styles.sectionSuratObservasiDataIndividu}>
        <Text style={styles.textName}>No.Hp</Text>
        <Text>:</Text>
        <Text style={styles.textKet}>{mail_noHp}</Text>
      </View>

      {/* View */}
      <View style={styles.sectionInformation}>
        {perihal === "Izin Penelitian" ? (
          <Text>
            Untuk mengadakan <Text style={styles.textItalic}>{perihal}</Text>{" "}
            dalam rangka penyusunan skripsi dengan judul{" "}
            <Text style={styles.textMatkul}>“{mail_matkul}”</Text> guna memenuhi
            sebagian persyaratan untuk mendapat gelar Sarjana{" "}
            {formatString(namafakultas).toCapitalizeEachWord()}. Hasil
            penelitian ini tidak akan dipublikasikan, melainkan semata-mata
            hanya untuk kepentingan ilmiah.
          </Text>
        ) : (
          <Text>
            Untuk mengadakan <Text style={styles.textItalic}>{perihal}</Text>{" "}
            dalam rangka memperdalam hal-hal yang berkenaan dengan mata kuliah{" "}
            <Text style={styles.textMatkul}>“{mail_matkul}”</Text> dan hasil
            kegiatan ini tidak akan dipublikasikan, melainkan semata-mata hanya
            untuk kepentingan ilmiah.
          </Text>
        )}
      </View>
    </GenerateSurat>
  );
};

const styles = StyleSheet.create({
  sectionSuratObservasiDataIndividu: {
    marginLeft: 40,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  sectionInformation: {
    textAlign: "justify",
    marginTop: 14,
  },
  textName: {
    flex: 0.75,
  },
  textKet: {
    flex: 1,
    textAlign: "justify",
    marginLeft: 5,
  },
  textItalic: {
    fontFamily: "Book Antiqua Italic",
  },
  textBold: {
    fontFamily: "Book Antiqua Bold",
  },
  textMatkul: {
    fontFamily: "Book Antiqua Bold",
    textTransform: "capitalize",
  },
});

export default Individu;
