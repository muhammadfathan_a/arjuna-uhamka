import GenerateSurat from "../GenerateSurat";
import { View, Text, StyleSheet } from "@react-pdf/renderer";
import { formatString } from "util/format";

const Kelompok = ({ lists, form }) => {
  const { perihal } = form;
  const { mail_matkul, namafakultas } = form.inputs;

  return (
    <GenerateSurat>
      <View style={styles.sectionHeaderSuratObservasiData}>
        <Text style={styles.textHeaderNo}>No</Text>
        <Text style={styles.textHeaderName}>Nama</Text>
        <Text style={styles.textHeaderNim}>Nim</Text>
        <Text style={styles.textHeaderProdi}>Prodi</Text>
        <Text style={styles.textHeaderSemester}>Semester</Text>
      </View>
      {lists === "NULL"
        ? window.alert("Something wrong")
        : lists.map((list, index) => {
            return (
              <View key={index} style={styles.sectionSuratObservasiData}>
                <Text style={styles.textMainNo}>{index + 1}.</Text>
                <Text style={styles.textMainName}>
                  {formatString(list.NAMA).toCapitalizeEachWord()}
                </Text>
                <Text style={styles.textMainNim}>{list.NIM}</Text>
                <Text style={styles.textMainProdi}>{list.NAMAPROGDI}</Text>
                <Text style={styles.textMainSemester}>{list.ANGKATAN}</Text>
              </View>
            );
          })}
      <View style={styles.sectionInformation}>
        {perihal === "Izin Penelitian" ? (
          <Text>
            Untuk mengadakan <Text style={styles.textItalic}>{perihal}</Text>{" "}
            dalam rangka penyusunan skripsi dengan judul{" "}
            <Text style={styles.textMatkul}>“{mail_matkul}”</Text> guna memenuhi
            sebagian persyaratan untuk mendapat gelar Sarjana{" "}
            {formatString(namafakultas).toCapitalizeEachWord()}. Hasil
            penelitian ini tidak akan dipublikasikan, melainkan semata-mata
            hanya untuk kepentingan ilmiah.
          </Text>
        ) : (
          <Text>
            Untuk mengadakan <Text style={styles.textItalic}>{perihal}</Text>{" "}
            dalam rangka memperdalam hal-hal yang berkenaan dengan mata kuliah{" "}
            <Text style={styles.textMatkul}>“{mail_matkul}”</Text> dan hasil
            kegiatan ini tidak akan dipublikasikan, melainkan semata-mata hanya
            untuk kepentingan ilmiah.
          </Text>
        )}
      </View>
    </GenerateSurat>
  );
};

const styles = StyleSheet.create({
  sectionHeaderSuratObservasiData: {
    flexDirection: "row",
    padding: 0,
    border: 1,
    fontSize: 11,
    fontFamily: "Book Antiqua Reguler",
  },
  sectionSuratObservasiData: {
    flexDirection: "row",
    padding: 0,
    borderBottom: 1,
    fontSize: 11,
  },
  sectionInformation: {
    textAlign: "justify",
    marginTop: 14,
  },
  //   Header
  textHeaderNo: {
    borderRight: 1,
    flex: 0.15,
    textAlign: "center",
    paddingHorizontal: 2,
  },
  textHeaderName: {
    borderRight: 1,
    flex: 0.8,
    textAlign: "center",
    paddingHorizontal: 2,
    textTransform: "capitalize",
  },
  textHeaderNim: {
    borderRight: 1,
    flex: 0.4,
    textAlign: "center",
    paddingHorizontal: 2,
  },
  textHeaderProdi: {
    borderRight: 1,
    flex: 0.6,
    textAlign: "center",
    paddingHorizontal: 2,
  },
  textHeaderSemester: {
    flex: 0.4,
    textAlign: "center",
    paddingHorizontal: 2,
  },

  //   Main
  textMainNo: {
    borderRight: 1,
    flex: 0.15,
    textAlign: "center",
    borderLeft: 1,
    paddingHorizontal: 2,
  },
  textMainName: {
    borderRight: 1,
    flex: 0.8,
    paddingHorizontal: 2,
  },
  textMainNim: {
    borderRight: 1,
    flex: 0.4,
    paddingHorizontal: 2,
    textAlign: "center",
  },
  textMainProdi: {
    borderRight: 1,
    flex: 0.6,
    paddingHorizontal: 2,
    textAlign: "center",
  },
  textMainSemester: {
    flex: 0.4,
    borderRight: 1,
    paddingHorizontal: 2,
    textAlign: "center",
  },
  textItalic: {
    fontFamily: "Book Antiqua Italic",
  },
  textBold: {
    fontFamily: "Book Antiqua Bold",
  },
  textMatkul: {
    fontFamily: "Book Antiqua Bold",
    textTransform: "capitalize",
  },
});

export default Kelompok;
