import React from "react";
import { Individu, Kelompok } from "../util";

const SuratPKL = ({ form }) => {
  const { mail_option, mail_anggotaKelompok } = form.inputs;

  const Surat =
    mail_option === "Individu" ? (
      <Individu form={form} />
    ) : (
      <Kelompok form={form} lists={mail_anggotaKelompok} />
    );

  return Surat;
};

export default SuratPKL;
