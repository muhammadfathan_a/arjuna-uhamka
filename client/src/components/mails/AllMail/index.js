import SuratIzinPenelitian from "./SuratIzinPenelitian";
import SuratUjiValiditas from "./SuratUjiValiditas";
import SuratObservasi from "./SuratObservasi";
import SuratPKL from "./SuratPKL";

export { SuratUjiValiditas, SuratObservasi, SuratIzinPenelitian, SuratPKL };
