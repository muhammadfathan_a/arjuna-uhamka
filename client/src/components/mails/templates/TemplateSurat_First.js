import {
  Document,
  Text,
  View,
  Page,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";
import {
  getDateNowM,
  getMonthNowM,
  getYearNowM,
  getIslamicDate,
} from "util/date";

import { formatString } from "util/format";

import BARegulerFont from "assets/font/BKANT.TTF";
import BABoldFont from "assets/font/ANTQUAB.TTF";
import BAItalicFont from "assets/font/ANTQUAI.TTF";

const TemplateSurat = ({ children, form }) => {
  const mail_alamatInstansi = form.inputs.mail_alamatInstansi;
  const mail_ditunjukan = form.inputs.mail_ditunjukan;
  const perihal = form.perihal;
  const namafakultas = form.inputs.namafakultas;

  // Tanggal Sekarang (Masehi)
  const tanggalSekarangMasehi = getDateNowM();
  const bulanSekarangMasehi = getMonthNowM();
  const tahunSekarangMasehi = getYearNowM();

  const { formatIDayMonth, iYear } = getIslamicDate();

  return (
    <Document className={styles.container}>
      <Page size="Legal" style={styles.page}>
        {/* Section Lampiran */}
        <View style={styles.sectionLampiran}>
          <View style={styles.sectionLampiranLeft}>
            <View style={styles.sectionLampiranLeftNama}>
              <Text>Nomor</Text>
              <Text>Lampiran</Text>
              <Text>Perihal</Text>
            </View>
            <View style={styles.sectionLampiranLeftKet}>
              <Text>: -</Text>
              <Text>: -</Text>
              <Text>: {perihal}</Text>
            </View>
          </View>
          <View style={styles.sectionLampiranRight}>
            <View style={styles.sectionLampiranRightHijriah}>
              <Text style={{ paddingRight: 15 }}>{formatIDayMonth}</Text>
              <Text>{iYear} H</Text>
            </View>
            <View style={styles.sectionLampiranRightMasehi}>
              <Text>
                {tanggalSekarangMasehi} {bulanSekarangMasehi}
              </Text>
              <Text>{tahunSekarangMasehi} M</Text>
            </View>
          </View>
        </View>
        {/* End Section Lampiran */}

        {/* Section Isi */}
        <View style={styles.sectionIsiSurat}>
          <Text>Yang terhormat,</Text>
          <Text style={styles.textTujuan}>{mail_ditunjukan}</Text>
          <Text>{mail_alamatInstansi}</Text>
          <Text style={styles.textSalam}>
            Assalamu’alaikum warahmatullahi wabarakatuh,
          </Text>
          <Text style={styles.textPembukaanSurat}>
            Pimpinan Fakultas{" "}
            {formatString(namafakultas).toCapitalizeEachWord()} Universitas
            Muhammadiyah Prof. DR. HAMKA mengharapkan kesediaan Bapak/Ibu
            kiranya berkenan untuk menerima dan memberikan izin kepada mahasiswa
            kami tersebut di bawah ini :
          </Text>
          {/* End Section Isi */}

          {/* Section Children */}
        </View>
        {/* End Section Children */}
        <View style={styles.sectionChildren}>{children}</View>

        <View style={styles.sectionFooter}>
          <Text style={{ textAlign: "justify" }}>
            Demikian permohonan ini kami sampaikan, atas perhatian dan perkenan
            diucapkan terima kasih.
          </Text>
          <View style={styles.sectionSalamFooter}>
            <Text>Wabillahit taufiq walhidayah,</Text>
            <Text>Wassalamu’alaikum warahmatullahi wabarakatuh,</Text>
          </View>
        </View>

        <View style={styles.sectionParaf}>
          <View style={{ marginLeft: 240 }}>
            <Text>a.n. Dekan</Text>
            <Text style={{ marginLeft: 20 }}>Wakil Dekan I,</Text>
            <Text style={{ paddingVertical: 20 }}></Text>
            <Text style={{ marginLeft: 20, fontFamily: "Book Antiqua Bold" }}>
              Dr. Dan Mugisidi, M.T
            </Text>
          </View>
        </View>

        {/* <Text>Tembusan :</Text>
        <Text>Dekan FKIP UHAMKA</Text> */}
      </Page>
    </Document>
  );
};

const styles = StyleSheet.create({
  page: {
    backgroundColor: "white",
    paddingTop: 96,
    paddingBottom: 96,
    paddingLeft: 96,
    paddingRight: 96,
    fontSize: 11,
    fontFamily: "Book Antiqua Reguler",
  },
  sectionLampiran: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  sectionLampiranLeft: {
    flexDirection: "row",
  },
  sectionLampiranLeftNama: {
    marginRight: 20,
  },
  sectionLampiranRight: {
    width: 120,
  },
  sectionLampiranRightHijriah: {
    flexDirection: "row",
    borderBottom: 1,
    justifyContent: "space-between",
  },
  sectionLampiranRightMasehi: {
    flexDirection: "row",
    justifyContent: "space-between",
  },

  sectionIsiSurat: {
    marginTop: 28,
    marginLeft: 73.6,
  },
  sectionChildren: {
    paddingLeft: 73.6,
    width: "100%",
    marginVertical: 14,
  },
  sectionFooter: { marginLeft: 73.6 },
  sectionSalamFooter: {
    fontFamily: "Book Antiqua Italic",
    marginVertical: 14,
  },
  sectionParaf: {
    marginBottom: 10,
  },

  textTujuan: {
    fontFamily: "Book Antiqua Bold",
  },
  textSalam: {
    fontFamily: "Book Antiqua Italic",
    marginTop: 10,
    marginBottom: 10,
  },
  textPembukaanSurat: {
    textAlign: "justify",
  },
});

Font.register({
  family: "Book Antiqua Reguler",
  src: BARegulerFont,
});
Font.register({
  family: "Book Antiqua Bold",
  src: BABoldFont,
});
Font.register({
  family: "Book Antiqua Italic",
  src: BAItalicFont,
});

export default TemplateSurat;
