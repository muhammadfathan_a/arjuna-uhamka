import React from "react";
import Template from "./template/Template";
import { Button, Card } from "../global";
import styled from "styled-components";

// import { CardInput } from "./template";
import { FiAlertCircle } from "react-icons/fi";

const KeteranganLulus = ({ data }) => {
  const handleSubmitKeteranganLulus = e => {
    e.preventDefault();

    console.log("ok");
  };

  return (
    <Template title={data.mail_list_name} autoCreate={true}>
      <KeteranganLulusStyled onSubmit={handleSubmitKeteranganLulus}>
        <Card className="alert-card">
          <p>This feature is not available yet</p>
          <FiAlertCircle style={{ fontSize: 20 }} />
        </Card>
        <div className="form-input">
          <Button
            type="submit"
            value="Kirim Permintaan"
            style={{ marginTop: 5 }}
            disabled={true}
          />
        </div>
      </KeteranganLulusStyled>
    </Template>
  );
};

const KeteranganLulusStyled = styled.form`
  .form-input {
    display: flex;
    flex-direction: column;
    margin-bottom: 12px;
  }

  .alert-card {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
    color: var(--mainBlue);
    font-weight: bold;

    & p {
      margin-right: 5px;
    }
  }
`;

export default KeteranganLulus;
