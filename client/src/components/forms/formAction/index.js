import React from "react";
// import { Link } from "react-router-dom";

import { Button } from "components/global";
import { validateFormData } from "../util";
import styled from "styled-components";

const FormAction = ({ option, inputs, perihal, onClick }) => {
  // const slug = perihal.split(" ").join("").toLowerCase();

  return (
    <FormActionStyled>
      <Button
        type="submit"
        value="Submit"
        style={{ width: "100%" }}
        disabled={!validateFormData(inputs).isValid}
        onClick={onClick}
      />
      {/* <div style={{ textAlign: "center", margin: "10px 0", fontSize: 14 }}>
        {option !== ""
          ? validateFormData(inputs).isValid && (
              <Link to={`/u/surat/surat${slug}.pdf`}>See Preview</Link>
            )
          : null}
      </div> */}
    </FormActionStyled>
  );
};

const FormActionStyled = styled.div`
  margin: 10px 0;
  width: 100%;
`;

export default FormAction;
