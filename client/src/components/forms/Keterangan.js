import React from "react";
import Template from "./template/Template";
import { FiAlertCircle } from "react-icons/fi";
import { Card } from "../global";
import styled from "styled-components";

const Keterangan = ({ data }) => {
  return (
    <Template title={data.mail_list_name} autoCreate={true}>
      <KeteranganStyled>
        <Card className="alert-card">
          <p>This feature is not available yet</p>
          <FiAlertCircle style={{ fontSize: 20 }} />
        </Card>
      </KeteranganStyled>
    </Template>
  );
};

const KeteranganStyled = styled.div`
  .alert-card {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
    color: var(--mainBlue);
    font-weight: bold;

    & p {
      margin-right: 5px;
    }
  }
`;

export default Keterangan;
