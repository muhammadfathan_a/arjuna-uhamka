import React from "react";

import FormInputs from "./formInputs";
import FormAction from "./formAction";
import Template from "./template/Template";
import { destructurDataKelompok } from "../forms/util";

const IzinPenelitian = ({ history, data, formContext, mailListId, user }) => {
  const { form, event } = formContext;
  const { nama, nim, namaprogdi, thakad, kodefak } = user;

  const handleSubmit = e => {
    e.preventDefault();

    const mail_anggotaKelompok =
      form.inputs.mail_anggotaKelompok === "NULL"
        ? "NULL"
        : destructurDataKelompok(form.inputs.mail_anggotaKelompok);

    const data = {
      ...form.inputs,
      mail_anggotaKelompok,
      idfakultas: kodefak,
      mail_mahasiswa: nama,
      mail_nim: nim,
      mail_progdi: namaprogdi,
      mail_smtak_thak: thakad,
      mail_list_id: mailListId,
    };

    const confirm = window.confirm(
      "Apakah seluruh data sudah benar? Anda tidak akan bisa kembali jika terdapat kesalahan data! Harap periksa kembali"
    );

    if (confirm) event.SendForm({ formData: data, history });
  };

  return (
    <Template title={data.mail_list_name}>
      <FormInputs formContext={formContext} perihal="Izin Penelitian" />
      {Object.keys(form).length !== 0 && (
        <FormAction
          inputs={form.inputs}
          perihal={form.perihal}
          onClick={handleSubmit}
        />
      )}
    </Template>
  );
};

export default IzinPenelitian;
