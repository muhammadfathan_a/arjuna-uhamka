import IzinPenelitian from "./IzinPenelitian";
import Keterangan from "./Keterangan";
import PKL from "./PKL";
import Observasi from "./Observasi";
import UjiValiditas from "./UjiValiditas";
import KeteranganLulus from "./KeteranganLulus";
import Cuti from "./Cuti";

export {
  IzinPenelitian,
  Keterangan,
  PKL,
  Observasi,
  UjiValiditas,
  KeteranganLulus,
  Cuti,
};
