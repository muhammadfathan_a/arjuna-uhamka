import React from "react";
import styled from "styled-components";

import { Card } from "components/global";

import { AiOutlinePlus, AiOutlineClose } from "react-icons/ai";

const CardInput = ({
  children,
  title,
  className,
  info,
  style,
  wajibDiIsi,
  onClose,
  canClose,
  isEmpty,
  isAddInput,
  onAddInput,
}) => {
  return (
    <CardInputStyled
      className={className}
      style={style}
      inputIsEmpty={isEmpty}
      isAddInput={isAddInput}
    >
      <Card className="cardInput-card">
        <div className="cardInput-title">
          <p style={{ fontSize: (14.5).toExponential }}>
            {title} {wajibDiIsi && <small style={{ color: "red" }}>*</small>}
          </p>

          {info && (
            <p style={{ fontSize: 12 }}>
              {info} <small style={{ color: "red" }}>*</small>
            </p>
          )}
        </div>
        <div className="cardInput-input">
          {/* children */}
          {children}
          {isEmpty && (
            <small style={{ color: "red", fontSize: 12 }}>
              Field masih kosong, harap di isi
            </small>
          )}
        </div>

        <div className="card-action-button">
          {canClose && (
            <div className="card-close-button" onClick={onClose}>
              <AiOutlineClose />
            </div>
          )}
          {isAddInput && (
            <div className="card-add-button" onClick={onAddInput}>
              <AiOutlinePlus />
            </div>
          )}
        </div>
      </Card>
    </CardInputStyled>
  );
};

const CardInputStyled = styled.div`
  margin-bottom: 12px;

  .cardInput-card {
    padding: 1.2rem 12px;
    height: 150px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    position: relative;
    box-shadow: 0 0 2px
      ${props => (props.inputIsEmpty ? "red" : "var(--black)")};
  }
  .cardInput-card:hover .card-action-button {
    z-index: 1;
    opacity: 1;
    right: -35px;
  }

  .cardInput-input {
    width: 100%;
  }

  .card-action-button {
    position: absolute;
    right: -40px;
    top: 0;
    height: 30px;
    width: 30px;
    cursor: pointer;
    border-radius: 0 2px 0 2px;
    opacity: 0;
    transition: all 0.3s ease-out;
  }

  .card-close-button,
  .card-add-button {
    background: var(--white);
    border-radius: 4px;
    box-shadow: 0 0 2px var(--black);
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: var(--black);
  }

  .card-close-button {
    margin-bottom: ${props => props.isAddInput && "5px"};
  }
`;

CardInput.defaultProps = {
  canClose: false,
};

export default CardInput;
