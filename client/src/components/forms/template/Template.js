import React from "react";
import styled from "styled-components";

import { Alert, Card } from "../../global";

const Template = ({ title, autoCreate, children }) => {
  return (
    <TemplateStyled>
      <Card className="template-header">
        <div className="card-header">
          <p>{title}</p>
          <p>* Wajib </p>
        </div>
      </Card>
      {!autoCreate && (
        <Card className="card-alert">
          <Alert type="alert" text="Perhatikan format yang tertera!" />
        </Card>
      )}
      {children}
      <p style={{ textAlign: "center", fontSize: "12px" }}>
        Formulir ini dibuat dalam Universitas Muhammadiyah Prof. Dr. Hamka
      </p>
    </TemplateStyled>
  );
};

const TemplateStyled = styled.div`
  .template-header {
    padding: 0;
    border-radius: 4px;
    margin-bottom: 1rem;
  }

  .card-header {
    border-top: 10px solid var(--mainBlue);
    border-radius: 2px 2px 0 0;
    color: var(--mainBlue);
    padding: 20px 16px;
  }
  .card-header p:nth-child(1) {
    font-weight: bold;
    font-size: 1.5rem;
  }
  .card-header p:nth-child(2) {
    color: red;
    font-size: 14px;
    margin-top: 1em;
  }
  .card-alert {
    margin-bottom: 12px;
    padding: 2px;
  }

  @media screen and (max-width: 689px) {
    .card-children {
      max-height: 550px;
    }
  }
`;

export default Template;
