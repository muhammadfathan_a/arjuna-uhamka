import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { AiFillCloseCircle } from "react-icons/ai";

import { useUser } from "contexts/userContext";

import { CardInput } from "../template";
import { JENIS_PILIHAN } from "@data/jenis_pilihan";
import { TextField, SelectField, Card } from "components/global";
import Button from "components/global/Button";
import Dialog from "components/global/Dialog";
import useDebounce from "util/debounce";

import { checkEmptyText, setDataInput } from "../util";

const FormInputs = ({
  formContext,
  isTtlMahsiswa,
  isNoHpMahasiswa,
  isAlamatMahasiswa,
  isOption,
  isDitunjukan,
  isAlamatInstansi,
  isMatkul,
  perihal,
}) => {
  const initialStateInput = { value: "", load: true };

  const [mail_option, setMailOption] = useState("");
  const [mail_ttl, setMailTTL] = useState(initialStateInput);
  const [mail_alamat, setMailAlamat] = useState(initialStateInput);
  const [mail_noHp, setMailnoHp] = useState(initialStateInput);
  const [mail_ditunjukan, setMailDitunjukan] = useState(initialStateInput);
  const [mail_alamatInstansi, setMailAlamatInstansi] = useState(
    initialStateInput
  );
  const [mail_matkul, setMailMatkul] = useState(initialStateInput);
  const [mail_anggotaKelompok, setMailAnggotaKelompok] = useState([]);

  const [openDialogTambahMahasiswa, setOpenDialogTambahMahasiswa] = useState(
    false
  );

  // Input Tanggal Lahir Mahasiswa --------------
  const handleChangeTanggalLahirMahasiswa = e => {
    const input = e.target.value;
    const objInput = { ...mail_ttl };
    objInput.value = input;
    objInput.load = false;

    setMailTTL(objInput);
  };
  // ------------------------------------

  // Input Alamat Mahasiswa --------------
  const handleChangeAlamatMahasiswa = e => {
    const input = e.target.value;
    const objInput = { ...mail_alamat };
    objInput.value = input;
    objInput.load = false;

    setMailAlamat(objInput);
  };
  // ------------------------------------

  // Input NoHpMahasiswa --------------
  const handleChangeNoHpMahasiswa = e => {
    const input = e.target.value;
    const objInput = { ...mail_noHp };
    objInput.value = input;
    objInput.load = false;

    setMailnoHp(objInput);
  };
  // ------------------------------------

  // Input Select Option --------------
  const handleChangeOption = e => {
    const input = e.target.value;

    setMailOption(input);

    if (input === "Kelompok") {
      setMailAnggotaKelompok([]);
    } else {
      if (mail_anggotaKelompok.length !== 0) {
        const confirm = window.confirm(
          "Kelompok sudah ada, data kelompok akan terhapus. Ingin lanjutkan?"
        );
        if (confirm) setMailAnggotaKelompok([]);
      }
    }
  };
  // ------------------------------------

  // Input Ditunjukan --------------
  const handleChangeDitunjukan = e => {
    const input = e.target.value;
    const objInput = { ...mail_ditunjukan };
    objInput.value = input;
    objInput.load = false;

    setMailDitunjukan(objInput);
  };
  // ------------------------------------

  // Input Alamat Instansi --------------
  const handleChangeAlamatInstansi = e => {
    const input = e.target.value;
    const objInput = { ...mail_alamatInstansi };
    objInput.value = input;
    objInput.load = false;
    setMailAlamatInstansi(objInput);
  };
  // ------------------------------------

  // Input Matakuliah --------------
  const handleChangeMatkul = e => {
    const input = e.target.value;
    const objInput = { ...mail_matkul };
    objInput.value = input;
    objInput.load = false;
    setMailMatkul(objInput);
  };
  // ------------------------------------

  const eventEffectDataInputForm = () => {
    const dataForPreview = setDataInput({
      type: "preview",
      data: {
        type: perihal,
        mail_mahasiswa: "Achmad Sufyan Aziz",
        mail_nim: 1703015175,
        mail_progdi: "Elektro",
        mail_smtak_thak: "V / 2019",
        mail_ttl: isTtlMahsiswa && mail_ttl,
        mail_alamat: isAlamatMahasiswa && mail_alamat,
        mail_noHp: isNoHpMahasiswa && mail_noHp,
        mail_option: isOption ? mail_option : "Individu",
        mail_ditunjukan: isDitunjukan && mail_ditunjukan,
        mail_alamatInstansi: isAlamatInstansi && mail_alamatInstansi,
        mail_matkul: isMatkul && mail_matkul,
        mail_anggotaKelompok,
        perihal,
      },
    });
    formContext.event.setInputForms(dataForPreview);
  };

  useEffect(eventEffectDataInputForm, [
    isOption,
    isAlamatInstansi,
    isNoHpMahasiswa,
    isTtlMahsiswa,
    isDitunjukan,
    isMatkul,
    isAlamatMahasiswa,
    perihal,
    mail_ttl,
    mail_alamat,
    mail_noHp,
    mail_option,
    mail_ditunjukan,
    mail_alamatInstansi,
    mail_matkul,
    mail_anggotaKelompok,
  ]);

  const eventEffectSetState = () => {
    const lengthForm = Object.keys(formContext.form).length;

    setMailTTL({
      value: lengthForm === 0 ? "" : formContext.form.inputs.mail_ttl,
      load: lengthForm === 0 ? true : false,
    });
    setMailAlamat({
      value: lengthForm === 0 ? "" : formContext.form.inputs.mail_alamat,
      load: lengthForm === 0 ? true : false,
    });
    setMailnoHp({
      value: lengthForm === 0 ? "" : formContext.form.inputs.mail_noHp,
      load: lengthForm === 0 ? true : false,
    });
    setMailDitunjukan({
      value: lengthForm === 0 ? "" : formContext.form.inputs.mail_ditunjukan,
      load: lengthForm === 0 ? true : false,
    });
    setMailAlamatInstansi({
      value:
        lengthForm === 0 ? "" : formContext.form.inputs.mail_alamatInstansi,
      load: lengthForm === 0 ? true : false,
    });
    setMailMatkul({
      value: lengthForm === 0 ? "" : formContext.form.inputs.mail_matkul,
      load: lengthForm === 0 ? true : false,
    });
    setMailAnggotaKelompok(
      lengthForm === 0 ? [] : formContext.form.inputs.mail_anggotaKelompok
    );
  };

  useEffect(eventEffectSetState, []);

  // Searchbar Dialog -------------------------------------
  const [searchNim, setSearchNim] = useState("");
  const [mahasiswa, setMahasiswa] = useState({});
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [clearHistorySearch, setClearHistorySearch] = useState(true);
  const debouncedSearchTerm = useDebounce(searchNim, 500);
  const { event } = useUser();

  const eventEffectDebounced = () => {
    if (debouncedSearchTerm) {
      setLoadingSearch(true);
      setClearHistorySearch(false);
      event.searchMahasiswa(debouncedSearchTerm).then(res => {
        if (res === undefined) {
          setTimeout(() => {
            setLoadingSearch(false);
            setMahasiswa({});
          }, 500);
        } else {
          setTimeout(() => {
            setLoadingSearch(false);
            setMahasiswa(res.data);
          }, 500);
        }
      });
    } else {
      setLoadingSearch(false);
      setMahasiswa({});
    }
  };

  useEffect(eventEffectDebounced, [debouncedSearchTerm, event]);

  const handleOpenDialogTambahMahasiswa = () => {
    setOpenDialogTambahMahasiswa(true);
    document.body.style.overflow = "hidden";
  };

  const handleCloseDialogTambahMahasiswa = () => {
    setClearHistorySearch(true);
    setOpenDialogTambahMahasiswa(false);
    setMahasiswa({});
    setLoadingSearch(false);
    setSearchNim("");
    document.body.style.overflow = "unset";
  };

  const handleChangeSearchNim = e => {
    const input = e.target.value;
    setSearchNim(input);
  };

  const handleTambahKelompok = () => {
    const nim = searchNim;
    const index = mail_anggotaKelompok.findIndex(val => val.NIM === nim);
    if (index !== -1) {
      alert("Mahasiswa sudah ditambahkan, kamu tidak bisa menambahkannya lagi");
    } else {
      handleCloseDialogTambahMahasiswa();
      setMailAnggotaKelompok(prevState => [...prevState, mahasiswa]);
    }
    // setMailAnggotaKelompok([mahasiswa]);
  };

  const handleRemoveKelompok = data => {
    const alertMsg = `Menghapus kelompok ${data.NAMA} (${data.NIM}) ?`;
    const confirm = window.confirm(alertMsg);
    if (confirm) {
      const filteredMahasiswa = mail_anggotaKelompok.filter(
        val => val.NIM !== data.NIM
      );
      setMailAnggotaKelompok(filteredMahasiswa);
    }
  };
  // ------------------------------------------------------

  // Dialog Search Mahasiswa
  const DialogSearchMahasiswa = (
    <Dialog
      onClose={handleCloseDialogTambahMahasiswa}
      open={openDialogTambahMahasiswa}
    >
      <DialogChildren>
        <div className="dialog-header">
          <p>
            <b>Tambah Kelompok</b>
          </p>
          <small style={{ fontSize: 10 }}>
            <b style={{ color: "red" }}>*</b> Harap masukan juga nim anda
          </small>
        </div>
        <div className="dialog-action">
          <TextField
            placeholder="Search Mahasiswa by Nim"
            onChange={handleChangeSearchNim}
            value={searchNim}
          />
          <div className="dialog-data">
            {clearHistorySearch ? (
              <div />
            ) : loadingSearch ? (
              <small>Searching Mahasiswa...</small>
            ) : Object.keys(mahasiswa).length === 0 ? (
              <small>Mahasiswa is not found</small>
            ) : (
              <div className="card-detail-mahasiswa">
                <small>
                  {mahasiswa.NAMA} ({mahasiswa.NIM})
                </small>
              </div>
            )}
          </div>
          <Button
            disabled={
              Object.keys(mahasiswa).length === 0 || loadingSearch === true
            }
            value="Tambah"
            style={{ width: "100%" }}
            onClick={handleTambahKelompok}
          />
        </div>
      </DialogChildren>
    </Dialog>
  );

  return (
    <FormInputsStyled>
      {/* Form Header */}
      <Card className="form-header">
        <p>Data Mahasiswa</p>
      </Card>

      {/* If Ttl is true */}
      {isTtlMahsiswa && (
        <CardInput
          title="Tanggal Lahir"
          info="Contoh : Jakarta, 17 Agustus 1998"
          wajibDiIsi={true}
          isEmpty={!mail_ttl.load && checkEmptyText(mail_ttl.value)}
        >
          <TextField
            onChange={handleChangeTanggalLahirMahasiswa}
            value={mail_ttl.value}
            placeholder="Jawaban Anda"
          />
        </CardInput>
      )}

      {/* If Alamat Mahasiswa */}
      {isAlamatMahasiswa && (
        <CardInput
          title="Alamat"
          wajibDiIsi={true}
          isEmpty={!mail_alamat.load && checkEmptyText(mail_alamat.value)}
        >
          <TextField
            onChange={handleChangeAlamatMahasiswa}
            value={mail_alamat.value}
            placeholder="Jawaban Anda"
          />
        </CardInput>
      )}

      {/* If NoHp Mahasiswa */}
      {isNoHpMahasiswa && (
        <CardInput
          title="No Hp"
          wajibDiIsi={true}
          isEmpty={!mail_noHp.load && checkEmptyText(mail_noHp.value)}
        >
          <TextField
            onChange={handleChangeNoHpMahasiswa}
            value={mail_noHp.value}
            placeholder="Jawaban Anda"
            type="number"
          />
        </CardInput>
      )}

      {/* Form Header */}
      <Card className="form-header">
        <p>Data Tujuan</p>
      </Card>

      {/* If Option True */}
      {isOption && (
        <CardInput title="Option" wajibDiIsi={true}>
          <SelectField onChange={handleChangeOption} value={mail_option}>
            {!mail_option && <option>Pilihan Anda</option>}
            {JENIS_PILIHAN.map((item, index) => {
              return (
                <option value={item.name} key={index}>
                  {item.name}
                </option>
              );
            })}
          </SelectField>
        </CardInput>
      )}

      {/* If Ditunjukan True */}
      {isDitunjukan && (
        <CardInput
          title="Ditunjukan Kepada"
          wajibDiIsi={true}
          isEmpty={
            !mail_ditunjukan.load && checkEmptyText(mail_ditunjukan.value)
          }
        >
          <TextField
            onChange={handleChangeDitunjukan}
            value={mail_ditunjukan.value}
            placeholder="Jawaban Anda"
          />
        </CardInput>
      )}

      {/* If Alamat Instansi is true */}
      {isAlamatInstansi && (
        <CardInput
          title="Alamat Instansi/Perusahaan Lengkap"
          wajibDiIsi={true}
          isEmpty={
            !mail_alamatInstansi.load &&
            checkEmptyText(mail_alamatInstansi.value)
          }
        >
          <TextField
            onChange={handleChangeAlamatInstansi}
            value={mail_alamatInstansi.value}
            placeholder="Jawaban Anda"
          />
        </CardInput>
      )}

      {/* If Alamat Instansi is true */}
      {isMatkul && (
        <CardInput
          title="Matakuliah"
          wajibDiIsi={true}
          isEmpty={!mail_matkul.load && checkEmptyText(mail_matkul.value)}
        >
          <TextField
            onChange={handleChangeMatkul}
            value={mail_matkul.value}
            placeholder="Jawaban Anda"
          />
        </CardInput>
      )}

      {/* If IsOption is true add Kelompok */}
      {isOption && mail_option === "Kelompok" && (
        <div className="form-card-kelompok">
          <div className="card-kelompok-header">
            <p>Kelompok</p>
            <div className="card-kelompok-button">
              <Button
                value="Tambah Kelompok"
                onClick={handleOpenDialogTambahMahasiswa}
              />
            </div>
          </div>
          <div className="card-kelompok-isi">
            <div className="card-kelompok-lists">
              {mail_anggotaKelompok.length === 0 ||
              mail_anggotaKelompok === "NULL" ? (
                <small>
                  Kelompok belum terbentuk, silahkan tambah kelompok!
                </small>
              ) : (
                mail_anggotaKelompok.map(val => {
                  return (
                    <div className="card-kelompok-list" key={val.NIM}>
                      <p>
                        {val.NAMA} ({val.NIM})
                      </p>
                      <AiFillCloseCircle
                        className="icon-close"
                        onClick={() => handleRemoveKelompok(val)}
                      />
                    </div>
                  );
                })
              )}
            </div>
          </div>
        </div>
      )}
      {/* Dialog Search Mahasiswa */}
      {DialogSearchMahasiswa}
      {/* ----------------------- */}
    </FormInputsStyled>
  );
};

const FormInputsStyled = styled.div`
  .form-header {
    margin-bottom: 10px;
    background: var(--mainBlue);
    color: white;
    font-weight: bold;
  }

  .form-card-kelompok {
    background: var(--white);
    border-radius: 2px;
    box-shadow: 0 0 2px var(--darkGray);

    & .card-kelompok-header {
      background: var(--mainBlue);
      color: var(--white);
      padding: 12px 30px;
      display: flex;
      justify-content: space-between;
      align-items: center;

      & p {
        font-weight: bold;
      }

      & button {
        background: var(--white);
        color: var(--mainBlue);
      }
    }

    & .card-kelompok-isi {
      padding: 10px 30px;

      & .card-kelompok-list {
        border: 1px solid var(--mainBlue);
        padding: 5px 10px;
        margin-bottom: 5px;
        display: flex;
        align-items: center;
        justify-content: space-between;
      }

      & .icon-close {
        color: var(--mainBlue);
        cursor: pointer;
        margin-left: 10px;
        font-size: 20px;
      }

      & .icon-close:hover {
        color: red;
      }
    }
  }
`;

const DialogChildren = styled.div`
  .dialog-header {
    margin-bottom: 10px;
  }

  .dialog-data {
    margin: 10px 0;
  }

  .card-detail-mahasiswa {
    padding: 10px;
    background: var(--white);
    border: 1px solid var(--mainBlue);
    color: var(--mainBlue);
    display: flex;
    align-items: center;
  }
`;

FormInputs.propTypes = {
  option: PropTypes.bool,
};

FormInputs.defaultProps = {
  isTtlMahsiswa: true,
  isAlamatMahasiswa: true,
  isNoHpMahasiswa: true,
  isOption: true,
  isDitunjukan: true,
  isAlamatInstansi: true,
  isMatkul: true,
  isWaktuPelaksanaan: true,
};

export default FormInputs;
