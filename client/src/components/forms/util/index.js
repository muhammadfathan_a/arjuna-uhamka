const isEmpty = value => {
  const getString = value.toString();
  return getString.trim() === "" ? true : false;
};

const checkEmptyObj = (mail_anggotaKelompok, option) => {
  if (option === "Individu") {
    return { isKelompokEmpty: false };
  } else {
    if (mail_anggotaKelompok === "NULL") return { isKelompokEmpty: true };
    else return { isKelompokEmpty: false };
  }
};

const checkEmptyValue = data => {
  let arr = [];
  let errors = {};

  Object.entries(data).forEach(([key, value]) => {
    arr.push(`${key}:${value}`);
  });

  arr.forEach(val => {
    const splitVal = val.split(":");
    const key = splitVal[0];
    const value = splitVal[1];
    const text = `${key} is empty`;

    if (isEmpty(value)) errors[key] = text;
  });

  const { isKelompokEmpty } = checkEmptyObj(
    data.mail_anggotaKelompok,
    data.mail_option
  );
  if (isKelompokEmpty) errors.kelompok = "Kelompok is empty";

  return {
    errors,
    isValid: Object.keys(errors).length === 0 ? true : false,
  };
};

export const handleRemoveKelompok = ({ i, data }) => {
  const filterDataKelompok = data.filter((val, valIndex) => valIndex !== i);
  const checkEmptyText = data[i];

  return {
    filterData: filterDataKelompok,
    checkEmptyText,
  };
};

export const validateFormData = data => {
  const { isValid, errors } = checkEmptyValue(data);

  return { isValid, errors };
};

export const checkEmptyText = val => {
  if (val.trim() === "") return true;
  else return false;
};

export const destructurDataKelompok = data => {
  const kelompok = [];
  data.map(val => {
    const tempVal = { ...val };
    tempVal.TGLAHIR = tempVal.TGLAHIR.split("T")[0];
    kelompok.push(tempVal);
  });
  return JSON.stringify(kelompok);
};

export const setDataInput = ({ type, data }) => {
  const mail_mahasiswa = data.mail_mahasiswa;
  const mail_nim = data.mail_nim;
  const mail_progdi = data.mail_progdi;
  const mail_smtak_thak = data.mail_smtak_thak;
  const mail_ttl = data.mail_ttl.value;
  const mail_alamat = data.mail_alamat.value;
  const mail_noHp = data.mail_noHp.value;
  const mail_option = data.mail_option;
  const mail_ditunjukan = data.mail_ditunjukan.value;
  const mail_alamatInstansi = data.mail_alamatInstansi.value;
  const mail_matkul = data.mail_matkul.value;
  const mail_anggotaKelompok = data.mail_anggotaKelompok;

  return {
    perihal: data.perihal,
    inputs: {
      mail_nim,
      mail_mahasiswa,
      mail_progdi,
      mail_smtak_thak,
      mail_ttl,
      mail_alamat,
      mail_noHp,
      mail_option,
      mail_ditunjukan,
      mail_alamatInstansi,
      mail_matkul,
      mail_anggotaKelompok:
        mail_anggotaKelompok.length === 0 ? "NULL" : mail_anggotaKelompok,
    },
  };
};
