import React from "react";
import Template from "./template/Template";
import { Button, Card } from "../global";
import styled from "styled-components";

// import { CardInput } from "./template";
import { FiAlertCircle } from "react-icons/fi";

const Cuti = ({ data }) => {
  const handleSubmitCuti = e => {
    e.preventDefault();
    console.log("ok");
  };

  return (
    <Template title={data.mail_list_name} autoCreate={true}>
      <CutiStyled onSubmit={handleSubmitCuti}>
        {/* <CardInput className="form-input" title="Izin Cuti" /> */}
        <Card className="alert-card">
          <p>This feature is not available yet</p>
          <FiAlertCircle style={{ fontSize: 20 }} />
        </Card>
        <div className="form-input">
          <Button
            type="submit"
            value="Kirim Izin Cuti"
            style={{ marginTop: 5 }}
            disabled={true}
          />
        </div>
      </CutiStyled>
    </Template>
  );
};

const CutiStyled = styled.form`
  .form-input {
    display: flex;
    flex-direction: column;
    margin-bottom: 12px;
  }
  .form-input label {
    margin-bottom: 8px;
  }

  .alert-card {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
    color: var(--mainBlue);
    font-weight: bold;

    & p {
      margin-right: 5px;
    }
  }
`;

export default Cuti;
