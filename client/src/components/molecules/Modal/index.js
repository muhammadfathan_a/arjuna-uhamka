import React from 'react';

const Modal = ({show,children}) => {
  return (
   <React.Fragment>
      <div className="modal__container">
        <div className={`modal__wrapper ${show ? 'modal__wrapper--show' : ''}`} >
          <div className="modal__inner">
            <div className="modal__title">
              <p>Tambahkan Catatan</p>
            </div>
            <div className="modal__box">
            {children}
            </div>
          </div>
        </div>
      </div>
   </React.Fragment>
  )
}

export default Modal;