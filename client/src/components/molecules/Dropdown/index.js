import {React} from 'libraries'

const Dropdown = () => {
  const [toggleDrop, setToggle] = React.useState(false);
  
  const handleDrop = () => {
    setToggle(!toggleDrop)
  }
  return(
    <div class="btn-group">
      <button 
        type="button"
        onClick={handleDrop}
        className={`btn__action ${toggleDrop ? 'btn__action--active' : '' }`}
      >
        Status
      </button>
      <div  id="dropdownMenu" className={`dropdownMenu ${toggleDrop ? 'dropdownMenu--active' : ''}`}>
        <div className="btn__item">
          <button className="dropdown-item" href="#">On Proses</button>
        </div>
        <div className="btn__item">
          <button className="dropdown-item" href="#">Di Tolak</button>
        </div>
        <div className="btn__item">
          <button className="dropdown-item" href="#">Selesai</button>
        </div>
      </div>  
    </div>
  )
}
export default Dropdown;