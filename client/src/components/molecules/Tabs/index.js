import {React, useState, useEffect} from 'libraries';

const Tabs = (props) => {
  const {children} = props;
  const [ tabHeader, setTabHeader] = useState([]);
  const [childContent, setChildContent] = useState({});
  const [active ,  setActive] = useState("");

  useEffect(()=>{
    const header = [];
    const childCnt = {};
    React.Children.forEach(children, (e) => {
      if(!React.isValidElement(e)) return;
      const {name} = e.props;
      header.push(name);
      childCnt[name] = e.props.children;
    });

    setTabHeader(header)
    setActive(header[0]);
    setChildContent({...childCnt});
    // console.log(childCnt);
  }, [props, children]);

  const changeTab = (name) =>{
    setActive(name);
  };

  return(
    <div className="tabs">
      <div className="tabs__header">
        {tabHeader.map((item, index) => (
          <button
            onClick= {()=> changeTab(item)}
            key ={item}
            className ={`tab__btn ${item === active ? "tabs__btn-active" : ""}`}
          > 
            {item}
          </button>
        ))}
      </div>
      <div className="tabs__content">
        {Object.keys(childContent).map((key, index) => {
          if(key === active){
            // console.log(key)
            return(
              <div key={index} className="tabs__child">
                {childContent[key]}{""}
              </div>
            );
          }else{
            return null;
          }
        })}
      </div>
    </div>
  )
}

export default Tabs;