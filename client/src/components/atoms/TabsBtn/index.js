import React from 'libraries';

const TabBtn = ({children}) =>{
  return(
    <div className="tab-btn">
      {children}
    </div>
  )
}

export default TabBtn;