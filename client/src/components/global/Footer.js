import React from "react";

import styled from "styled-components";

const Footer = () => {
  const getYearNow = new Date().getFullYear();

  return (
    <FooterStyled>
      <p>Get In Touch BPTI</p>
      <p>COPYRIGHT &copy; {getYearNow} | BPTI Uhamka </p>
    </FooterStyled>
  );
};

const FooterStyled = styled.div`
  display: flex;
  justify-content: space-between;
  color: var(--mainBlue);

  @media screen and (max-width: 689px) {
    p {
      flex: 1;
      font-size: 14px;
    }
  }
`;

export default Footer;
