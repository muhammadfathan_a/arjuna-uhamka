import React from "react";
import Spinner from "./Spinner";

import styled from "styled-components";

const Loading = ({
  typeSpinner,
  colorSpinner,
  sizeSpinner,
  className,
  style,
  fullScreen,
}) => {
  return (
    <LoadingStyled className={className} style={style} fullScreen={fullScreen}>
      <Spinner
        type={typeSpinner}
        size={sizeSpinner}
        color={colorSpinner}
        style={{ display: "inline-block" }}
      />
      <small>We try to get data, Please wait...</small>
    </LoadingStyled>
  );
};

const LoadingStyled = styled.div`
  height: ${props => (props.fullScreen ? "100vh" : "auto")};
  width: ${props => (props.fullScreen ? "100vw" : "auto")};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
  border-radius: 4px;
  flex-direction: column;
`;

export default Loading;
