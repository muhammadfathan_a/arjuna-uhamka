import React from "react";

import styled from "styled-components";

const TextField = ({
  value,
  onChange,
  className,
  style,
  placeholder,
  name,
  type,
}) => {
  return (
    <TextFieldStyled
      value={value}
      onChange={onChange}
      className={className}
      style={style}
      placeholder={placeholder}
      name={name}
      type={type}
    />
  );
};

const TextFieldStyled = styled.input`
  padding: 6px 10px;
  width: 100%;
`;

export default TextField;
