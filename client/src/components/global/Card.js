import React from "react";
import styled from "styled-components";

const Card = ({ className, style, children }) => {
  return (
    <CardStyled className={className} style={style}>
      {children}
    </CardStyled>
  );
};

const CardStyled = styled.div`
  background: var(--white);
  height: auto;
  border-radius: 2px;
  box-shadow: 0 0 2px var(--darkGray);
  padding: 12px 30px;
`;

export default Card;
