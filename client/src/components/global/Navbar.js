import React from "react";
import styled from "styled-components";
import { NavLink, withRouter } from "react-router-dom";

import { Card } from "components/global";
import { useUser } from "contexts/userContext";

const Navbar = props => {
  const { event } = useUser();

  const logout = () => {
    const confirm = window.confirm("Yakin ingin keluar?");
    if (confirm) event.userLogout({ history: props.history });
  };

  return (
    <NavbarStyled>
      <Card className="navbar-container">
        <div className="navbar-links">
          <NavLink to="/u/home">Home</NavLink>
          <NavLink to="/u/status">Status</NavLink>
        </div>
        <div className="navbar-logout" onClick={logout}>
          <p>Keluar</p>
        </div>
      </Card>
    </NavbarStyled>
  );
};

const NavbarStyled = styled.div`
  margin-bottom: 16px;
  position: sticky;
  top: 0;
  z-index: 10;

  .navbar-container,
  .navbar-links {
    display: flex;
    justify-content: space-between;
  }

  .navbar-links a {
    margin-right: 40px;
    text-decoration: none;
    color: var(--black);
  }
  .navbar-links a.active {
    color: var(--mainBlue);
    text-decoration: underline;
  }

  .navbar-logout {
    cursor: pointer;
  }
  .navbar-logout:hover {
    color: var(--mainBlue);
  }
`;

export default withRouter(Navbar);
