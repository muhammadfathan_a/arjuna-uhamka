import React from "react";

import Card from "./Card";

import styled from "styled-components";

const Dialog = ({
  onClick,
  className,
  classNameChild,
  style,
  children,
  onClose,
  open,
}) => {
  const scrollY = window.scrollY;

  return (
    <React.Fragment>
      <StyledDialog
        pageHeight={scrollY}
        className={className}
        style={style}
        onClick={onClose}
        open={open}
      />
      <StyledDialogChildren
        className={classNameChild}
        pageHeight={scrollY}
        open={open}
      >
        <Card className="dialog-card">{children}</Card>
      </StyledDialogChildren>
    </React.Fragment>
  );
};

const StyledDialog = styled.div`
  position: absolute;
  top: ${props => props.pageHeight}px;
  right: 0;
  background: rgba(0, 52, 102, 0.8);
  height: 100vh;
  width: 100%;
  z-index: 10;
  transition: opacity 0.7s ease-in-out;
  ${props =>
    props.open
      ? { visibility: "visible", opacity: 1 }
      : { visibility: "hidden", opacity: 0.6 }}
`;

const StyledDialogChildren = styled.div`
  position: absolute;
  top: ${props => props.pageHeight}px;
  z-index: 11;
  background: red;
  left: 50%;
  margin-top: 140px;
  transition: transform 0.5s ease-out;
  ${props =>
    props.open
      ? { transform: "translate(-50%,0)", visibility: "visible" }
      : { transform: "translate(-50%,-180%)", visibility: "hidden" }}
  .dialog-card {
    width: 500px;
    height: auto;
  }
`;

export default Dialog;
