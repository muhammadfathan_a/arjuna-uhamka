import React from "react";

import styled from "styled-components";

const Button = ({ value, onClick, className, style, type, disabled }) => {
  return (
    <ButtonStyled
      onClick={onClick}
      className={className}
      style={style}
      type={type}
      disabled={disabled}
    >
      {value}
    </ButtonStyled>
  );
};

const ButtonStyled = styled.button`
  padding: 10px 5px;
  border-radius: 4px;
  background: ${props => (props.disabled ? "gray" : "var(--mainBlue)")};
  border: none;
  outline: none;
  color: var(--white);
  cursor: ${props => (props.disabled ? "not-allowed" : "pointer")};
`;

export default Button;
