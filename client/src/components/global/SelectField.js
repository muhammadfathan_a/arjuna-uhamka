import React from "react";

import styled from "styled-components";

const SelectField = ({ value, onChange, className, style, children }) => {
  return (
    <SelectFieldStyled
      value={value}
      onChange={onChange}
      className={className}
      style={style}
    >
      {children}
    </SelectFieldStyled>
  );
};

const SelectFieldStyled = styled.select`
  padding: 6px;
  width: 100%;
`;

export default SelectField;
