import React from "react";
import PropsTypes from "prop-types";

import {
  CircleLoader,
  BarLoader,
  BounceLoader,
  BeatLoader,
} from "react-spinners";

const Spinner = ({ type, style, color, size, loading }) => {
  // switch type
  switch (type) {
    case "circle":
      return (
        <CircleLoader css={style} color={color} size={size} loading={loading} />
      );
    case "bar":
      return (
        <BarLoader css={style} color={color} size={size} loading={loading} />
      );
    case "bounce":
      return (
        <BounceLoader css={style} color={color} size={size} loading={loading} />
      );
    case "beat":
      return (
        <BeatLoader css={style} color={color} size={size} loading={loading} />
      );
    default:
      return;
  }
};

Spinner.propTypes = {
  type: PropsTypes.oneOf(["circle", "bar", "bounce", "beat"]),
  className: PropsTypes.string,
  style: PropsTypes.object,
};

Spinner.defaultProps = {
  type: "circle",
};

export default Spinner;
