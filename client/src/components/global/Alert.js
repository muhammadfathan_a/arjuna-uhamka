import React from "react";

import styled from "styled-components";

import { GoAlert } from "react-icons/go";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { BiError } from "react-icons/bi";

const Alert = ({ type, text, onClick, style }) => {
  const SwitchIcon = () => {
    switch (type) {
      case "alert":
        return <GoAlert className="alert-icon" />;
      case "success":
        return <AiOutlineCheckCircle className="alert-icon" />;
      case "errors":
        return <BiError className="alert-icon" />;
      default:
        return;
    }
  };

  const Icon = SwitchIcon();

  return (
    <AlertStyled type={type} onClick={onClick} style={style}>
      {Icon}
      <div className="alert-text">
        <small>{text}</small>
      </div>
    </AlertStyled>
  );
};

const AlertStyled = styled.div`
  display: flex;
  padding: 6px 12px;
  align-items: center;
  ${props => props.type === "alert" && "background: #fff4e6;"}
  ${props => props.type === "success" && "background: #edf7ee;"}
  ${props => props.type === "errors" && "background: #ec4646;"}

  ${props => props.type === "alert" && "color: var(--darkGray);"}
  ${props => props.type === "success" && "color: var(--darkGray);"}
  ${props => props.type === "errors" && "color: var(--white);"}

  
  .alert-text {
    margin-right: 10px;
  }

  .alert-icon {
    margin-right: 16px;
    font-size: 20px;
    ${props => props.type === "alert" && "color: #ffb763;"}
    ${props => props.type === "success" && "color: #b6e8b0;"}
    ${props => props.type === "errors" && "color: var(--white);"}
  }
`;

export default Alert;
