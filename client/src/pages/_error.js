import React, { useEffect } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { AiOutlineSearch } from "react-icons/ai";

const ErrorPage = ({ title, subtitle, status, type, path }) => {
  useEffect(() => {
    document.title = "Not Found - Si Pesat";
  }, []);

  const component =
    type === "error-page" ? (
      <ErrorPageStyled>
        <div className="error-status">
          <h1>Status Page {status}</h1>
          <p></p>
        </div>
        <div className="error-title">
          <p>"{title}"</p>
          <p>{subtitle}</p>
        </div>
      </ErrorPageStyled>
    ) : (
      type === "error-url" && (
        <ErrorPageStyled>
          <div className="error-status">
            <h1>Uhamka Si Pesat | Status Page 404</h1>
            <p></p>
          </div>
          <div className="error-title">
            <p style={{ display: "flex", alignItems: "center" }}>
              <AiOutlineSearch style={{ marginRight: 8 }} /> We can't find what
              you want
            </p>
          </div>
          <Link to={path} style={{ marginTop: 10, color: "red" }}>
            Back to main page
          </Link>
        </ErrorPageStyled>
      )
    );

  return component;
};

const ErrorPageStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  color: var(--mainBlue);
  .error-status {
    margin-bottom: 10px;
  }
`;

export default ErrorPage;
