import React, { useEffect } from "react";

import Loading from "components/global/Loading";
import ErrorPage from "pages/_error";

import { useForm } from "contexts/formContext";

import PageLayout from "layout/PageLayout";
import { Link } from "react-router-dom";

import styled from "styled-components";
import { Card } from "components/global";

export const Home = props => {
  const { mailList, loadingForm, event, errorForm } = useForm();

  useEffect(() => {
    document.title = "Sistem Pengajuan Surat - Home";
    document.body.style.overflow = "unset";
    event.DeleteInputForms();
  }, []);

  useEffect(() => {
    if (!loadingForm) {
      if (mailList.length === 0) {
        event.GetMailList();
      }
    }
  }, []);

  useEffect(() => {
    if (Object.keys(errorForm).length !== 0) {
      window.alert("❌ Something wrong with server or db ❌");
    }
  }, [errorForm]);

  return loadingForm ? (
    <Loading
      typeSpinner="beat"
      colorSpinner={"var(--mainBlue)"}
      sizeSpinner={30}
      fullScreen
    />
  ) : Object.keys(errorForm).length !== 0 ? (
    <ErrorPage
      title={errorForm.message}
      status={errorForm.status}
      type="error-page"
    />
  ) : (
    <PageLayout>
      <UserHomeStyled>
        <Card className="list-main-header">
          <p>Pilih Jenis Surat</p>
        </Card>
        <div className="list-main-container">
          {mailList
            .sort((a, b) => (a.mail_list_name > b.mail_list_name ? 1 : -1))
            .map((list, index) => {
              return (
                <Link
                  className="list-card"
                  key={list.mail_list_id}
                  to={`/u/form?id=${list.mail_list_id}`}
                >
                  <p>{index + 1}</p>
                  <p>{list.mail_list_name}</p>
                </Link>
              );
            })}
        </div>
      </UserHomeStyled>
    </PageLayout>
  );
};

const UserHomeStyled = styled.div`
  margin-bottom: 16px;

  .list-main-header {
    margin-bottom: 12px;
    font-weight: bold;
  }

  .list-card {
    background: var(--white);
    box-shadow: 0 0 1px var(--black);
    border-radius: 2px;
    text-decoration: none;
    display: flex;
    height: 80px;
    align-items: center;
    margin-bottom: 10px;
    text-align: center;
    cursor: pointer;
    transform: transalteY(0);
    transition: transform 0.5s ease-out;
    color: var(--black);
  }
  .list-card:hover {
    transform: translateY(-2px);
    box-shadow: 0 0 2px var(--black);
  }
  .list-card:nth-child(even) {
    background: var(--gray);
  }

  .list-card p:nth-child(1) {
    flex: 0.2;
    font-size: 1.5em;
    font-weight: bold;
    color: var(--mainBlue);
  }
  .list-card p:nth-child(2) {
    flex: 1;
  }
`;

export default Home;
