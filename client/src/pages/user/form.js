import React, { useEffect } from "react";
import queryString from "querystring";
import styled from "styled-components";

import { useUser } from "contexts/userContext";
import { useForm } from "contexts/formContext";

import { Card } from "components/global";
import {
  IzinPenelitian,
  Keterangan,
  KeteranganLulus,
  PKL,
  Observasi,
  UjiValiditas,
  Cuti,
} from "components/forms";

import { FiAlertCircle } from "react-icons/fi";

const Form = props => {
  const search = queryString.parse(props.location.search);
  const paramsIdValue = Object.values(search)[0];
  const id = Number(paramsIdValue);
  const formContext = useForm();
  const { details } = useUser();

  const filterSurat = formContext.mailList.filter(
    list => list.mail_list_id === id
  );
  const dataSurat = filterSurat[0];
  const formExist = filterSurat.length !== 0 ? true : false;

  useEffect(() => {
    document.title = "Sistem Pengajuan Surat - Form/id=" + id;
  }, [id]);

  const SwitchForm = () => {
    if (dataSurat === undefined) {
      return;
    } else {
      switch (dataSurat.mail_list_id) {
        case 1:
          return (
            <Keterangan
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 2:
          return (
            <IzinPenelitian
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 3:
          return (
            <KeteranganLulus
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 4:
          return (
            <Observasi
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 5:
          return (
            <UjiValiditas
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 6:
          return (
            <Cuti
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        case 7:
          return (
            <PKL
              formContext={formContext}
              data={dataSurat}
              history={props.history}
              mailListId={id}
              user={details}
            />
          );
        default:
          return;
      }
    }
  };

  const componentForm = SwitchForm();

  return (
    <FormStyled exist={formExist}>
      {!formExist ? (
        <Card>
          <h2 style={{ display: "flex", alignItems: "center" }}>
            <FiAlertCircle style={{ marginRight: 10 }} /> 404 | Form is Not
            Found <FiAlertCircle style={{ marginLeft: 10 }} />
          </h2>
        </Card>
      ) : (
        componentForm
      )}
    </FormStyled>
  );
};

const FormStyled = styled.div`
  background: var(--lightGray);
  min-height: 100vh;
  width: 100%;
  padding: 20px 350px;
  display: ${props => !props.exist && "flex"};
  align-items: ${props => !props.exist && "center"};
  justify-content: ${props => !props.exist && "center"};

  @media screen and (max-width: 990px) {
    padding: 20px 150px;
  }
  @media screen and (max-width: 689px) {
    padding: 20px 12px;
  }
`;

export default Form;
