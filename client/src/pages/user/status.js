import React, { useEffect } from "react";

import PageLayout from "layout/PageLayout";
import { Link } from "react-router-dom";

import { useUser } from "contexts/userContext";
import { useForm } from "contexts/formContext";
import styled from "styled-components";
import { Card } from "components/global";
import { getSlugMail } from "util/mail-list";
import { convertDate } from "util/date";

const Status = () => {
  const { event, statusMailUser, loadingUser } = useUser();
  const {
    event: { setMailToInput },
  } = useForm();

  const checkEmptyStatus = false;

  useEffect(() => {
    document.title = "Sistem Pengajuan Surat - Status Surat";
  }, []);

  useEffect(() => {
    if (!loadingUser) {
      if (statusMailUser.length === 0) {
        event.getStatusMailUser();
      }
    }
  }, [statusMailUser, loadingUser]);

  return (
    <PageLayout>
      <StatusStyled checkEmptyStatus={checkEmptyStatus}>
        <Card className="status-card">
          {checkEmptyStatus ? (
            <p>
              <i>Kamu belum mengajukan surat apapun !</i>
            </p>
          ) : statusMailUser.length === 0 ? (
            <p
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "100%",
              }}
            >
              Kamu belum mengajukan surat apapun
            </p>
          ) : (
            <div className="status-container-card">
              <div
                className="status-header-card"
                style={{ borderBottom: "1px solid black" }}
              >
                <p>Nama Surat</p>
                <p>Tanggal Pengajuan</p>
                <p>Status</p>
                <p>Surat</p>
                <p>Note</p>
              </div>

              {statusMailUser.map(status => {
                // get slug for url surat
                const slug = getSlugMail(status.mail_list_name);

                return (
                  <div key={status.mail_status_id} className="status-list-card">
                    <p style={{ fontSize: 14 }}>{status.mail_list_name}</p>
                    <p style={{ fontSize: 14 }}>
                      {convertDate(status.mail_created)}
                    </p>
                    <p
                      className={`text-${status.mail_status_status}`}
                      style={{ fontSize: 14 }}
                    >
                      {status.mail_status_status}
                    </p>
                    <Link
                      to={`/u/surat/surat${slug}.pdf`}
                      style={{ fontSize: 14 }}
                      // setMailToInput buat conver jadi -> {perihal: "nama surat", inputs:{status}}
                      onClick={() => setMailToInput(status)}
                    >
                      view
                    </Link>
                    <p style={{ fontSize: 14 }}>
                      {status.mail_status_keterangan}
                    </p>
                    {""}
                  </div>
                );
              })}
            </div>
          )}
        </Card>
      </StatusStyled>
    </PageLayout>
  );
};

const StatusStyled = styled.div`
  height: 300px;

  .status-card {
    height: 290px;
    overflow-y: auto;
    ${props => (props.checkEmptyStatus ? "display:flex" : "")};
    ${props => (props.checkEmptyStatus ? "align-items:center" : "")};
    ${props => (props.checkEmptyStatus ? "justify-content:center" : "")};
    padding: 0;
  }

  .status-container-card {
    position: relative;
  }
  .status-header-card {
    position: sticky;
    top: 0;
    background: var(--white);
  }

  .status-header-card,
  .status-list-card {
    display: flex;
    padding: 16px 14px;
    text-align: center;
  }

  .status-header-card {
    font-weight: bold;
  }

  .status-header-card p:nth-child(1),
  .status-list-card p:nth-child(1) {
    flex: 1;
  }
  .status-header-card p:nth-child(2),
  .status-list-card p:nth-child(2) {
    flex: 1;
    padding-left: 10px;
  }
  .status-header-card p:nth-child(3),
  .status-list-card p:nth-child(3) {
    flex: 0.5;
    padding-left: 10px;
  }
  .status-header-card p:nth-child(4),
  .status-list-card a {
    flex: 0.5;
    padding-left: 10px;
  }
  .status-header-card p:nth-child(5),
  .status-list-card p:nth-child(5) {
    flex: 1;
    padding-left: 10px;
  }

  .text-Diproses {
    color: var(--mainBlue);
  }
  .text-Diterima {
    color: green;
  }
  .text-Ditolak {
    color: red;
  }
`;

export default Status;
