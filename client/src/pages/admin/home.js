import { useEffect} from "react";
import { NavbarAdmin, TableOnProses,TableFailed,TableCompleted,Tabs,TabsBtn} from "components";
import React from "react";
import LogoWhite from "../../assets/images/UHAMKA-White.png";
import { useAdmin } from "contexts/adminContext";

const Home = () => {
  const { allMails, event, errors, loadingAdmin } = useAdmin();

  useEffect(() => {
    if (Object.keys(errors).length === 0) {
      if (loadingAdmin === false) {
        if (allMails.length === 0) {
          event.getAllMail();
        }
      }
    }
  }, [event]);


  const filterDataSucess = allMails.filter(
    item => item.mail_status_status === "Diterima"
  )
  const filterDataGagal = allMails.filter(
    item => item.mail_status_status === "Ditolak"
  )
  const filterDataOnProses = allMails.filter(
    item => item.mail_status_status === "Diproses"
  )
  return (
    <div className="admin__page">
      <div className="container_admin">
        <div className="admin__wrapper">
          <div className="admin__inner">
            <section className="brand">
              <div className="brand__logo">
                <img src={LogoWhite} alt="Uhamka Logo"/>
              </div>
              <div className="brand__title">Sistem Pengajuan Surat Online</div>
            </section>
            <section className="navbar__container">
              <NavbarAdmin />
            </section>
            <section className="table__container">
              <div className="table__header">
                <div className="table__title">
                  <h1>List Permohonan</h1>
                </div>
              </div>
              <div className="table__body">
                  <Tabs>
                    <TabsBtn name ="On Proses" key="1">
                      <div className="table_item">
                        <div className="table__title">
                          <h1>List Permohonan Di Proses</h1>
                        </div>
                        <TableOnProses
                          filter = {filterDataOnProses}
                          allMails={allMails}
                        />
                      </div>
                    </TabsBtn>
                    <TabsBtn name ="Selesai" key="2">
                      <div className="table_item">
                        <div className="table__title">
                          <h1>List Permohonan Selesai</h1>
                        </div>
                        <TableCompleted 
                          filter={filterDataSucess} 
                          allMails={allMails} 
                        />
                      </div>
                    </TabsBtn>
                    <TabsBtn name ="Gagal" key="3">
                      <div className="table_item">
                        <div className="table__title">
                          <h1>List Permohonan DiTolak</h1>
                        </div>
                        <TableFailed
                          filter={filterDataGagal} 
                          allMails={allMails}
                        />
                      </div>
                    </TabsBtn>
                  </Tabs>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;