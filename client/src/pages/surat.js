import React, { useEffect } from "react";
import { useForm } from "contexts/formContext";

import {
  SuratUjiValiditas,
  SuratObservasi,
  SuratIzinPenelitian,
  SuratPKL,
} from "components/mails/AllMail";
import {
  SURAT_UJI_VALIDITAS,
  SURAT_OBSERVASI,
  SURAT_PKL,
  SURAT_IZIN_PENELITIAN,
} from "util/surat";

import { Card } from "components/global";
import { FiAlertCircle } from "react-icons/fi";

const Surat = ({ history, match }) => {
  const id_form = match.params.id_form;
  const { form } = useForm();
  const findForm = Object.keys(form).length === 0 ? false : true;

  useEffect(() => {
    document.title = `Sistem Pengajuan Surat - ${id_form}`;
  }, [id_form]);

  const SwitchSurat = () => {
    if (id_form === undefined) {
      return;
    } else {
      switch (id_form) {
        case SURAT_IZIN_PENELITIAN:
          return <SuratIzinPenelitian form={form} />;
        case SURAT_OBSERVASI:
          return <SuratObservasi form={form} />;
        case SURAT_UJI_VALIDITAS:
          return <SuratUjiValiditas form={form} />;
        case SURAT_PKL:
          return <SuratPKL form={form} />;
        default:
          return;
      }
    }
  };

  const SuratToPdf = SwitchSurat();

  return findForm ? (
    SuratToPdf
  ) : (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        background: "var(--lightGray)",
      }}
    >
      <Card>
        <h2 style={{ display: "flex", alignItems: "center" }}>
          <FiAlertCircle style={{ marginRight: 10 }} /> 404 | Surat is Not Found{" "}
          <FiAlertCircle style={{ marginLeft: 10 }} />
        </h2>
      </Card>
    </div>
  );
};

export default Surat;
