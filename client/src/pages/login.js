import React from "libraries";
import LogoWhite from "assets/images/UHAMKA-White.png";
import { LoginForm } from "components";
const Login = props => {
  return (
    <div className="LoginContainer">
      <div className="login">
        <div className="login__inner">
          <div className="row">
            <section className="loginSide">
              <div className="login_wrapper">
                <div className="login__logo">
                  <img src={LogoWhite} alt="UHAMKA LOGO"/>
                </div>
                <div className="login__desc">
                  <div className="desc__brand">
                    <h1>Si Pesat</h1>
                  </div>
                  <div className="desc__instruction">
                    <h5>Login untuk mengajukan surat</h5>
                  </div>
                </div>
              </div>
            </section>
            <section className="loginForm">
              <LoginForm history={props.history} />
            </section>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
