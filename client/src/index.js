import React from "react";
import ReactDOM from "react-dom";
import "./assets/scss/index.scss";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import { AdminProvider } from "contexts/adminContext";
import { UserProvider } from "contexts/userContext";
import { FormProvider } from "contexts/formContext";

import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <AdminProvider>
    <UserProvider>
      <FormProvider>
        <Router>
          <App />
        </Router>
      </FormProvider>
    </UserProvider>
  </AdminProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
