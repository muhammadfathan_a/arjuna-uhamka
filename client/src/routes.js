import React from "react";
import Loadable from "react-loadable";
import { BounceLoader } from "react-spinners";

const initialStyle = {
  height: "100vh",
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const Loading = props => {
  if (props.error) {
    return (
      <div style={initialStyle}>
        Error! <button onClick={props.retry}>Retry</button>
      </div>
    );
  } else if (props.timedOut) {
    return (
      <div style={initialStyle}>
        Taking a long time... <button onClick={props.retry}>Retry</button>
      </div>
    );
  } else if (props.pastDelay) {
    return (
      <div style={initialStyle}>
        <BounceLoader color={"var(--mainBlue)"} size={100} />
      </div>
    );
  } else {
    return null;
  }
};

const UserHome = Loadable({
  loader: () => import("pages/user/home"),
  loading: Loading,
});
const UserStatus = Loadable({
  loader: () => import("pages/user/status"),
  loading: Loading,
});
const UserForm = Loadable({
  loader: () => import("pages/user/form"),
  loading: Loading,
});
const AdminHome = Loadable({
  loader: () => import("pages/admin/home"),
  loading: Loading,
});
const Surat = Loadable({
  loader: () => import("pages/surat"),
  loading: Loading,
});
const Login = Loadable({
  loader: () => import("pages/login"),
  loading: Loading,
});
const ErrorPage = Loadable({
  loader: () => import("pages/_error"),
  loading: Loading,
});

const routes = [
  // ------- User Route
  {
    path: "/u/home",
    exact: true,
    name: "UserHome",
    component: UserHome,
    loginFor: "user",
  },
  {
    path: "/u/status",
    exact: true,
    name: "UserStatus",
    component: UserStatus,
    loginFor: "user",
  },
  {
    path: "/u/form",
    exact: true,
    name: "UserForm",
    component: UserForm,
    loginFor: "user",
  },
  {
    path: "/u/surat/:id_form",
    exact: true,
    name: "Surat",
    component: Surat,
    loginFor: "user",
  },
  
  // --------- Admin Route
  {
    path: "/a/home",
    exact: true,
    name: "AdminHome",
    component: AdminHome,
    loginFor: "admin",
  },
  {
    path: "/a/surat/:id_form",
    exact: true,
    name: "Surat",
    component: Surat,
    loginFor: "admin",
  },
  // Login Route
  {
    path: "/",
    exact: true,
    name: "Login",
    component: Login,
    loginFor: "-",
  },
  // Error Route
  {
    path: "*",
    exact: true,
    name: "ErrorPage",
    component: ErrorPage,
    loginFor: "-",
  },
];

export default routes;
