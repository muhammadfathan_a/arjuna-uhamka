import Axios from "axios";
import { useState, createContext, useContext } from "react";
import { instanceMainApi as axios } from "util/axios";

const AdminContext = createContext();

const setAuthorization = data => {
  const sipesat_token = `Bearer ${data.token}`;
  localStorage.setItem("uhamka_sipesat_login", JSON.stringify(data));
  Axios.defaults.headers.common["Authorization"] = sipesat_token;
};

export const AdminProvider = props => {
  const [detailAdmin, setDetailAdmin] = useState({});
  const [loadingAdmin, setLoadingAdmin] = useState(false);
  const [errors, setErrors] = useState({});
  const [allMails, setAllMails] = useState([]);
  const [statusMail, setStatusMail] = useState([]);

  const initialError = {
    message: "something wrong, check your server and connection",
    status: 500,
  };
  //Function Login
  const adminLogin = ({ admin_name, admin_password, history }) => {
    setLoadingAdmin(true);
    axios.post("/admin/login", {admin_name, admin_password})
    .then(res => {
      const data = {
        token : res.data.token,
        loginFor : res.data.loginFor
      };
      setAuthorization(data)
      history.push("/a/home");
      setErrors({})
      setLoadingAdmin(false)
    })
    .catch( error => {
      setLoadingAdmin(false);
      if(error.respone === undefined){
        setErrors(initialError)
      }else{
        setErrors(error.response.data);
        if (error.response.data.status === 401) {
          window.alert("Unauthorization or missing token!");
        }
      }
    }
    )
  }
  
  //Function update status Mail
  const updateStatus = ({mail_status_id, mail_status_status, mail_status_keterangan}) => {
    setLoadingAdmin(true);
    axios.post(`/admin/status_mail/update/${mail_status_id}`, {mail_status_status, mail_status_keterangan})
    .then(res =>{
      const data = {
        statusMail : res.data.statusMail,
        noteMail : res.data.noteMail
      };
      setStatusMail(data)
      // history.push("/a/home");
      setErrors({})
      // setLoadingAdmin(false)
    })
    .catch( error => {
      // setLoadingAdmin(false);
      if(error.respone === undefined){
        setErrors(initialError)
      } else{
        setErrors(error.response.data);
          if (error.response.data.status === 401) {
            window.alert("Unauthorization or missing token!");
          }
        };
      }
    )
  };
  //Function get Data Admin
  const getDataAdmin = () => {
    setLoadingAdmin(true);
    axios.get("/admin/data_admin")
    .then( res => {
      setDetailAdmin(res.data.data)
      setLoadingAdmin(false);
      setErrors({});
    })
    .catch(error => {
      setLoadingAdmin(false)
      if(error.response === undefined){
        setErrors(initialError)
      }else {
        setErrors(error.response.data)
      }
    });
  }

  //get All Mail
  const getAllMail = () => {
    setLoadingAdmin(true);
    axios
      .get("/admin/status_mail")
      .then(res => {
        setLoadingAdmin(false);
        setErrors({});
        setAllMails(res.data.data);
      })
      .catch(error => {
        setLoadingAdmin(false);
        if (error.respone === undefined) {
          setErrors(initialError);
        } else {
          setErrors(error.respone.data);
        }
      });
  };

  //Function Logout
  const adminLogout = ({ history }) => {
    localStorage.removeItem("uhamka_sipesat_login");
    delete axios.defaults.headers.common["Authorization"];
    // history.push("/");
    window.location.href = "/";
  };
  return (
    <AdminContext.Provider
      value={{
        detailAdmin,
        loadingAdmin,
        errors,
        allMails,
        statusMail,
        event : {adminLogin, adminLogout, getDataAdmin, getAllMail,updateStatus}
      }}
    >
      {props.children}
    </AdminContext.Provider>
  );
};

export const useAdmin = () => useContext(AdminContext);
