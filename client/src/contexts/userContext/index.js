import { useState, createContext, useContext } from "react";
import { instanceMainApi as axios } from "util/axios";

const UserContext = createContext();

const setAuthorization = data => {
  const sipesat_token = `Bearer ${data.token}`;
  localStorage.setItem("uhamka_sipesat_login", JSON.stringify(data));
  axios.defaults.headers.common["Authorization"] = sipesat_token;
};

export const UserProvider = props => {
  const [details, setDetails] = useState({});
  const [loadingUser, setLoadingUser] = useState(false);
  const [errors, setErrors] = useState({});
  const [statusMailUser, setStatusMailUser] = useState([]);

  const initialError = {
    message: "something wrong, check your server and connection",
    status: 500,
  };

  const userLogin = ({ nim, pwd, history }) => {
    setLoadingUser(true);
    axios
      .post("/mhs/login", { nim, pwd })
      .then(res => {
        const data = {
          token: res.data.token,
          loginFor: res.data.loginFor,
        };
        setAuthorization(data);
        history.push("/u/home");
        setErrors({});
        setLoadingUser(false);
      })
      .catch(error => {
        setLoadingUser(false);
        if (error.response === undefined) setErrors(initialError);
        else {
          console.log(error.response.data)
          if (error.response.data.status === 401) {
            window.alert("Unauthorization or missing token!");
          }
        }
      });
  };

  const getDataMahasiswa = () => {
    setLoadingUser(true);
    axios
      .get("/mhs/data_mahasiswa")
      .then(res => {
        setDetails(res.data.data);
        setLoadingUser(false);
        setErrors({});
      })
      .catch(error => {
        setLoadingUser(false);
        if (error.response === undefined) setErrors(initialError);
        else setErrors(error.response.data);
      });
  };

  const getStatusMailUser = () => {
    setLoadingUser(true);
    axios
      .get("/mail/status_mahasiswa")
      .then(res => {
        setStatusMailUser(res.data.data);
        setLoadingUser(false);
        setErrors({});
      })
      .catch(error => {
        setLoadingUser(false);
        if (error.response === undefined) setErrors(initialError);
        else setErrors(error.response.data);
      });
  };

  const searchMahasiswa = async nim => {
    const source = `/mhs/search/${nim}`;
    try {
      const result = await axios.get(source);
      return result.data;
    } catch (error) {}
  };

  const clearErrors = () => {
    setErrors({});
  };

  const userLogout = ({ history }) => {
    localStorage.removeItem("uhamka_sipesat_login");
    delete axios.defaults.headers.common["Authorization"];
    history.push("/");
    window.location.href = "/";
  };

  return (
    <UserContext.Provider
      value={{
        details,
        loadingUser,
        errors,
        statusMailUser,
        event: {
          userLogin,
          getDataMahasiswa,
          getStatusMailUser,
          userLogout,
          searchMahasiswa,
          clearErrors,
        },
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
