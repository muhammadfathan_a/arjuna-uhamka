import { useState, createContext, useContext } from "react";
import { useUser } from "../userContext";
import { instanceMainApi as axios } from "util/axios";
import { getPerihalMail } from "util/mail-list";

const FormContext = createContext();

export const FormProvider = props => {
  const userContext = useUser();
  const [forms, setForms] = useState({});
  const [mailList, setMailList] = useState([]);
  const [loadingForm, setLoadingForm] = useState(false);
  const [errorForm, setErrorForm] = useState({});
  const [alertForm, setAlertForm] = useState("");
  const initialError = {
    message: "something wrong, check your server and connection",
    status: 500,
  };

  // Start Section Forms
  const setInputForms = input => {
    setForms(input);
  };
  const DeleteInputForms = () => {
    setForms({});
  };
  const setMailToInput = list => {
    // change text to JSON
    const mail_anggotaKelompok =
      list.mail_anggotaKelompok === "NULL"
        ? "NULL"
        : JSON.parse(list.mail_anggotaKelompok);

    const inputs = { ...list, mail_anggotaKelompok };

    const data = {
      perihal: getPerihalMail(list.mail_list_name),
      inputs,
    };
    setForms(data);
  };
  // End Section Forms

  // alertForm
  const setAlertFormVal = val => {
    setAlertForm(val);
  };
  const DeleteAlertFormVal = () => {
    setAlertForm("");
  };
  // end alertForm

  // Section Mail
  const GetMailList = () => {
    setLoadingForm(true);
    axios
      .get("/mail/list")
      .then(res => {
        setMailList(res.data.data);
        setLoadingForm(false);
      })
      .catch(error => {
        setLoadingForm(false);
        if (error.response === undefined) setErrorForm(initialError);
        else {
          setErrorForm(error.response.data);
          if (error.response.data.status === 401) {
            window.alert("Unauthorization or missing token!");
          }
          window.alert("Something went wrong ❗");
        }
      });
  };
  // Send Form -------------
  const SendForm = ({ formData, history }) => {
    setLoadingForm(true);
    axios
      .post("/mail/send", formData)
      .then(res => {
        setLoadingForm(false);
        setAlertFormVal(
          "Sukses mengirimkan surat, surat akan dibalas secepat mungkin!"
        );
        userContext.event.getStatusMailUser();
        history.push("/u/home");
      })
      .catch(error => {
        setLoadingForm(false);
        if (error.response === undefined) setErrorForm(initialError);
        else {
          setErrorForm(error.response.data);
          if (error.response.data.status === 401) {
            window.alert("Unauthorization or missing token!");
          }
          window.alert("Something went wrong ❗");
        }
      });
  };

  return (
    <FormContext.Provider
      value={{
        form: forms,
        mailList,
        loadingForm,
        errorForm,
        alertForm,
        event: {
          setInputForms,
          DeleteInputForms,
          GetMailList,
          SendForm,
          DeleteAlertFormVal,
          setMailToInput,
        },
      }}
    >
      {props.children}
    </FormContext.Provider>
  );
};

export const useForm = () => useContext(FormContext);
