export const getPerihalMail = string => {
  const split_listname = string.split(" ");
  return split_listname.slice(1, split_listname.length - 1).join(" ");
};

export const getSlugMail = string => {
  const split_listname = string.split(" ");
  return split_listname
    .slice(1, split_listname.length - 1)
    .join("")
    .toLowerCase();
};
