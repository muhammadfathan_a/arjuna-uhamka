export const formatString = str => {
  return {
    toCapitalizeEachWord: () => formatCapitalizeEachWord(str),
  };
};

const formatCapitalizeEachWord = string => {
  return string
    .split(" ")
    .map(str => str.charAt(0).toUpperCase() + str.slice(1).toLowerCase())
    .join(" ");
};
