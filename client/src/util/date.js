import moment from "moment";
import momentHijri from "moment-hijri";

const Months = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

const IslamicMonths = [
  "Muharram",
  "Shafar",
  "Rabi'ul Awal",
  "Rabi'ul Akhir",
  "Jumadil Awal",
  "Jumadil Akhir",
  "Rajab",
  "Sya'ban",
  "Ramadhan",
  "Syawal",
  "Zulqaidah",
  "Zulhijjah ",
];

const date = new Date();

export const getDateNowM = () => {
  return date.getDate();
};

export const getMonthNowM = () => {
  const month = Months[date.getMonth()];
  return month;
};

export const getYearNowM = () => {
  return date.getFullYear();
};

export const convertDate = val => {
  return moment(val).format("LLL");
};

export const getIslamicMonth = bulan => {
  const month = IslamicMonths[bulan];
  return month;
};

export const getIslamicDate = () => {
  const day = momentHijri().iDate();
  const month = momentHijri().iMonth();
  const year = momentHijri().iYear();

  const monthNow = getIslamicMonth(month);

  return {
    iDay: day,
    iMonth: monthNow,
    iYear: year,
    formatIFullDate: `${day} ${monthNow} ${year}H`,
    formatIDayMonth: `${day} ${monthNow}`,
  };
};
