export const LIST_SURAT = [
  { id: 1, name: "Surat Keterangan (Individu/Kelompok)" },
  { id: 2, name: "Surat Izin Penelitian (Individu/Kelompok)" },
  { id: 3, name: "Surat Keterangan Lulus (Individu)" },
  { id: 4, name: "Surat Observasi (Individu/Kelompok)" },
  { id: 5, name: "Surat Uji Validitas (Individu)" },
  { id: 6, name: "Surat Cuti (Individu)" },
  { id: 7, name: "Surat PKL (Individu/Kelompok)" },
];
