import React, {Component} from 'react';
import PropTypes from 'prop-types';

export {
  React,
  Component,
  PropTypes
};

export * from 'react';
export * from 'react-router-dom';