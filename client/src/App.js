import React, { useEffect } from "react";
import { Switch, Route, Redirect, Link } from "react-router-dom";
import jwtDecode from "jwt-decode";

import { useAdmin } from "contexts/adminContext";
import { useUser } from "contexts/userContext";

import { instanceMainApi as axios } from "util/axios";

import routes from "routes";

const App = () => {
  const storage = localStorage.getItem("uhamka_sipesat_login");

  const userContext = useUser();
  const AdminContext = useAdmin();

  const PrivateRoute = ({ component: Component, ...rest }) => {
    if (storage == null || storage === "") {
      return <Redirect to="/" exact />;
    } else {
      const parseLocalStorage = JSON.parse(storage);
      let token = jwtDecode(parseLocalStorage.token);
      if (token.exp <= new Date().getTime() / 1000) {
        localStorage.removeItem("uhamka_sipesat_login");
        return <Redirect to="/" exact />;
      } else {
        const authToken = `Bearer ${parseLocalStorage.token}`;
        axios.defaults.headers.common["Authorization"] = authToken;
        if (parseLocalStorage.loginFor !== "mahasiswa") {
          return <ForbiddenPage loginFor={parseLocalStorage.loginFor} />;
        } else {
          return <Route {...rest} component={Component} />;
        }
      }
    }
  };

  const ForbiddenRoute = ({ component: Component, ...rest }) => {
    if (storage == null || storage === "") {
      return <Redirect to="/" exact />;
    } else {
      const parseLocalStorage = JSON.parse(storage);
      let token = jwtDecode(parseLocalStorage.token);
      if (token.exp <= new Date().getTime() / 1000) {
        localStorage.removeItem("uhamka_sipesat_login");
        return <Redirect to="/" exact />;
      } else {
        const authToken = `Bearer ${parseLocalStorage.token}`;
        axios.defaults.headers.common["Authorization"] = authToken;
        if (parseLocalStorage.loginFor !== "admin") {
          return <ForbiddenPage loginFor={parseLocalStorage.loginFor} />;
        } else {
          return <Route {...rest} component={Component} />;
        }
      }
    }
  };

  const PublicRoute = ({ component: Component, ...rest }) => {
    if (storage == null || storage === "") {
      return <Route {...rest} component={Component} />;
    } else {
      const parseLocalStorage = JSON.parse(storage);
      if (parseLocalStorage.loginFor === "mahasiswa") {
        return <Redirect from="/" to="/u/home" exact />;
      } else if (parseLocalStorage.loginFor === "admin") {
        return <Redirect from="/" to="/a/home" exact />;
      }
    }
  };

  const ErrorRoute = ({ component: Component, ...rest }) => {
    if (storage == null || storage === "") {
      return (
        <Route
          {...rest}
          component={props => (
            <Component {...props} path="/" type="error-url" />
          )}
        />
      );
    } else {
      const parseLocalStorage = JSON.parse(storage);
      if (parseLocalStorage.loginFor === "mahasiswa") {
        return (
          <Route
            {...rest}
            component={props => (
              <Component {...props} path="/u/home" type="error-url" />
            )}
          />
        );
      } else if (parseLocalStorage.loginFor === "admin") {
        return (
          <Route
            {...rest}
            component={props => (
              <Component {...props} path="/a/home" type="error-url" />
            )}
          />
        );
      }
    }
  };

  const ForbiddenPage = ({ loginFor }) => {
    document.title = "Sistem Pengajuan Surat - Forbidden (403)";
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
          flexDirection: "column",
        }}
      >
        <p style={{ fontSize: 24, marginBottom: 10 }}>
          <b>403</b> | Forbidden Access
        </p>
        <small>
          You have no access here,{" "}
          <Link to={loginFor === "mahasiswa" ? "/u/home" : "/a/home"}>
            go to main page!
          </Link>
        </small>
      </div>
    );
  };

  const eventEffectRoute = () => {
    if (storage == null || storage === "") {
      return <Redirect to="/" exact />;
    } else {
      const parseLocalStorage = JSON.parse(storage);
      if (parseLocalStorage.token.exp <= new Date().getTime() / 1000) {
        localStorage.removeItem("uhamka_sipesat_login");
        return <Redirect to="/" exact />;
      } else {
        const authToken = `Bearer ${parseLocalStorage.token}`;
        axios.defaults.headers.common["Authorization"] = authToken;
        if (parseLocalStorage.loginFor === "mahasiswa") {
          userContext.event.getDataMahasiswa();
          return <Redirect to="/u/home" />;
        }
        if (parseLocalStorage.loginFor === "admin") {
          AdminContext.event.getDataAdmin();
          return <Redirect to="/a/home" />;
        }
      }
    }
  };

  useEffect(eventEffectRoute, [storage]);

  return (
    <div className="app">
      <Switch>
        {routes.map(route => {
          return route.path === "/" ? (
            <PublicRoute
              exact={route.exact}
              path={route.path}
              name={route.name}
              key={route.name}
              component={route.component}
            />
          ) : route.path === "*" ? (
            <ErrorRoute
              exact={route.exact}
              path={route.path}
              name={route.name}
              key={route.name}
              component={route.component}
            />
          ) : route.loginFor === "user" ? (
            <PrivateRoute
              exact={route.exact}
              path={route.path}
              name={route.name}
              key={route.name}
              component={route.component}
            />
          ) : (
            <ForbiddenRoute
              exact={route.exact}
              path={route.path}
              name={route.name}
              key={route.name}
              component={route.component}
            />
          );
        })}
      </Switch>
    </div>
  );
};

export default App;
