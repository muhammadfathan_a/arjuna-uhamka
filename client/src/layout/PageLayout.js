import React, { useEffect, useRef } from "react";

import styled from "styled-components";
import LOGOUHAMKA from "assets/images/UHAMKA-White.png";

import { useUser } from "contexts/userContext";
import { useForm } from "contexts/formContext";
import { Card, Footer, Navbar, Alert } from "components/global";

const PageLayout = ({ history, children, className, style }) => {
  const { details, loadingUser } = useUser();
  const { alertForm, event } = useForm();
  const pageLayoutRef = useRef(null);

  const namafakultas =
    Object.keys(details).length !== 0 && details.namafakultas.toLowerCase();
  const namaprogdi =
    Object.keys(details).length !== 0 && details.namaprogdi.toLowerCase();

  useEffect(() => {
    if (alertForm !== "") {
      setTimeout(() => event.DeleteAlertFormVal(), 5000);
    }
    window.scrollTo({
      top: 0,
    });
  }, [alertForm]);

  return (
    <PageLayoutStyled ref={pageLayoutRef} className={className} style={style}>
      {/* profilecard */}
      {loadingUser ? (
        <div />
      ) : (
        <div>
          <ProfileCardStyled>
            <Card className="profile-container">
              <div className="profile-image">
                <img src={LOGOUHAMKA} alt="Logo Uhamka" />
              </div>
              <div className="profile-credentials">
                <p>{details.nama}</p>
                <p style={{ marginBottom: 10 }}>{details.nim}</p>
                <p>Fakultas {namafakultas}</p>
                <small>program studi {namaprogdi}</small>
              </div>
            </Card>
          </ProfileCardStyled>

          {alertForm === "" ? null : (
            <Alert
              type="success"
              text={alertForm}
              style={{ marginBottom: 14, boxShadow: "0 0 1px black" }}
            />
          )}

          {/* navbar */}
          <Navbar />

          {/* children */}
          {children}

          {/* footer */}
          <Footer />
        </div>
      )}
    </PageLayoutStyled>
  );
};

const PageLayoutStyled = styled.div`
  background: var(--lightGray);
  min-height: 100vh;
  padding: 20px 200px;

  @media screen and (max-width: 990px) {
    padding: 20px 100px;
  }
  @media screen and (max-width: 689px) {
    padding: 20px;
  }
`;
const ProfileCardStyled = styled.div`
  margin-bottom: 16px;

  .profile-container {
    display: flex;
    align-items: center;
    background: var(--mainBlue);

    color: var(--white);
  }
  .profile-image {
    margin-left: 45px;
    margin-right: 64px;
  }
  .profile-image img {
    width: 150px;
    height: 100%;
  }

  .profile-credentials {
    text-transform: capitalize;
  }

  .profile-credentials p:nth-child(1) {
    font-size: 24px;
    font-weight: bold;
  }

  @media screen and (max-width: 990px) {
    .profile-image {
      margin-left: 5px;
      margin-right: 20px;
    }
    .profile-credentials p:nth-child(1) {
      font-size: 18px;
    }
    .profile-credentials p:nth-child(2),
    .profile-credentials p:nth-child(3) {
      font-size: 14px;
    }
    .profile-image img {
      width: 100px;
      height: 100%;
    }
  }
`;

export default PageLayout;
