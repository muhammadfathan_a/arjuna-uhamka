# Penggunaan API

> base URL **http://localhost:4000/si_pesat/api**

## Mahasiswa API

> base URL mahasiswa **/mhs**

<table>
  <tr>
    <th>API</th>
    <th>Type</th>
    <th>Request</th>
  </tr>
  <tr>
    <td>
        /login
    </td>
    <td>
        POST
    </td>
    <td>
    {nim: number, pwd: string}
    </td>
  </tr>
  <tr>
    <td>
        /data_mahasiswa
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
</table>

## Admin API

> base URL admin **/admin**

<table>
  <tr>
    <th>API</th>
    <th>Type</th>
    <th>Request</th>
  </tr>
  <tr>
    <td>
        /login
    </td>
    <td>
        POST
    </td>
    <td>
    {admin_name: string, admin_password: string}
    </td>
  </tr>
  <tr>
    <td>
        /data_admin
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /list_admin
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /status_mail
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /status_mail/update/:id
    </td>
    <td>
        POST
    </td>
    <td>
    {mail_status_status: string,
    mail_status_keterangan: string}
    </td>
  </tr>
  <tr>
    <td>
        /change_password
    </td>
    <td>
        POST
    </td>
    <td>
    {new_password: string,
    re_password: string}
    </td>
  </tr>
</table>

## Mail API

> base URL mail **/mail**

<table>
  <tr>
    <th>API</th>
    <th>Type</th>
    <th>Request</th>
  </tr>
  <tr>
    <td>
        /list
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /list/:id
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /status_mahasiswa
    </td>
    <td>
        GET
    </td>
    <td>
        null
    </td>
  </tr>
  <tr>
    <td>
        /send
    </td>
    <td>
        POST
    </td>
    <td>
        {<br/>
            mail_list_id: number,
        <br/>
        idfakultas: string,
        <br/>
        mail_nim: number,
        <br/>
        mail_mahasiswa: string,
        <br/>
        mail_ttl: string,
        <br/>
        mail_alamat: string,
        <br/>
        mail_noHp: number,
        <br/>
        mail_progdi: string,
        <br/>
        mail_smtak_thak: string,
        <br/>
        mail_option: string,
        <br/>
        mail_ditunjukan: string,
        <br/>
        mail_alamatInstansi: string,
        <br/>
        mail_matkul: string,
        <br/>
        mail_anggotaKelompok: text -> JSON.stringfy()
        <br/>}
    </td>
  </tr>
 
</table>

> You can message me for more information ^\_^
